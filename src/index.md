---
title: Django / DRF
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---

- [Introduction](00-intro.md)
- [Python fondamentaux](00-python-fondamentaux.md)
- [Initiation à Django](01-django-initiation.md)
- [Django comme une app web](01-django-app-web.md)
- [Les outils de Django](02-outils-de-django.md)
- [Django intermédiaire](03-django-intermediaire.md)
- [Django Rest Framework (partie 1)](04-drf-initiation.md)
- [Django Rest Framework (partie 2)](05-drf-intermediaire.md)
