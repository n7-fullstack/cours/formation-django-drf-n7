

<!-- s -->

# Ma première API : une vue Json

<!-- v -->

## Vue API root

Notre API doit répondre à l'url `/api/v0/`

Django passe automatiquement une [HttpRequest](https://docs.djangoproject.com/fr/3.0/ref/request-response/#httprequest-objects) 
à notre vue.

```python
#polls/views.py
def api_root_view(request):
    return HttpResponse("Bienvenue dans l'API")
```
```python
#polls/urls.py
urlpatterns = [
    #…
    path('api/v0/', views.api_root, name='api_root'),
]
```

<!-- v -->

### Le test

(à titre indicatif)

```python
class TestPollAPI(TestCase):
    # ...
    def test_get_api_root(self):
        response = self.client.get("/api/v0/")
        self.assertContains(response, "Bienvenue dans l'API")

    def test_get_polls(self):
        Question.objects.create(question_text="Quelle boisson ?")
        Question.objects.create(question_text="Votre framework préféré ?")

        response = self.client.get("/api/v0/polls/")

        self.assertContains(response, "Quelle boisson ?")
        self.assertContains(response, "Votre framework préféré ?")
```

Le test `test_get_polls` plantent, c'est normal, nous n'avons encore rien codé à par lui

<!-- v -->

## Vue liste

Fonction de vue qui renvoie la liste des livres au format `Json` sur `/api/v0/books/`

```python
#polls/views.py
def api_polls(request):
    if request.method == "GET":
        questions = Question.objects.all()
        questions_data = [{
          'id': q.id,
          'question_text': q.question_text,
        } for q in questions]
        response_data = {"results": questions_data}
        return JsonResponse(response_data)
```

<!-- v -->

### Urls

```python
# bookstore/urls.py
urlpatterns = [
    # ...
    path('api/v0/polls/', views.api_polls, name='api_polls'),
]
```

<!-- v -->

## Premiers pas

Une approche par le haut

### Serializer

```python
# polls/serializers.py
from rest_framework import serializers

class BookSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Book
        fields = ('title')
```

<!-- v -->

## ViewSet

```python
# polls/views.py
from rest_framework import viewsets

class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all().order_by("title")
    serializer_class = BookSerializer
```

<!-- v -->

## Configuration des URLs

```python
# polls/urls.py
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'polls', views.BookViewSet)

urlpatterns = [
    # ...
    path('api/v1/', include(router.urls)),
]
```

<!-- v -->

## Tester

Exécuter les tests automatisés.

Tester avec curl :

```console
$ curl -v http://localhost:8000/api/v1/books/
```

Visiter [http://localhost:8000/api/v1/](http://localhost:8000/api/v1/) avec son navigateur.

<!-- v -->

## Pagination

```python
# bistrot_project/settings.py
REST_FRAMEWORK = {
  'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
  'PAGE_SIZE': 5
}
```
