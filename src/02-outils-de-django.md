---
title: Les outils de Django pour les tests
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---

# Outils pour le dev et les tests

<!-- s -->

## Django Debug Toolbar et Django extensions

<!-- v -->

## Installer une app extérieure&nbsp;:<br>Django Debug Toolbar

Django Debug Toolbar est une barre d'outil qui donne des infos de debug
relative à la requête et à la réponse en cours.

Suivre les [instructions d'installation](https://django-debug-toolbar.readthedocs.io/en/stable/installation.html).

<!-- v -->

![Capture d'écran Django Debug Toolbar](img/ddt.png)

<!-- s -->

# Écrire des tests, pourquoi et comment

<!-- v -->

## Principaux types de test

### Test unitaire

- Tests sur chaque composant de façon indépendant.
- Faciles à automatiser
- python : unittest

<!-- v -->

### Test d'intégration

- Tests sur les liens entre les composants
- Assez faciles à automatiser
- Django tests

<!-- v -->

### Test système ou fonctionnels

- Tests sur les fonctionnalités
- Pas toujours facile à automatiser
- Django tests

<!-- v -->

### Tests "end to end"

- Scénario sur un parcours d'utilisation de bout en bout
- On vient de passer à l'ère Gutemberg en terme de tests "e2e" automatisés
- [Cypress](https://www.cypress.io/)

<!-- v -->

## Autres types de tests

- Tests de performance
- Tests d'intrusion → sécurité
- Tests utilisateurs → pour les IHM
- "Monkey tests"

<!-- v -->

## Pourquoi les tests ?

- Garantir une application fiable
- Éviter les régressions au fil du temps
- Gérer les mises à jour sereinement
- Gagner du temps dans le développement

<!-- s -->

# Tests automatisés dans Django

<!-- v -->

## Les outils

- la classe TestCase de Django
- le client permettant de gérer les requêtes et les réponses

[Documentation sur les tests](https://docs.djangoproject.com/fr/3.0/topics/testing/), [Tutoriel](https://docs.djangoproject.com/fr/3.0/intro/tutorial05/)

<!-- v -->

## Ça ressemble à…

```bash
from django.test import TestCase
from myapp.models import Animal

class AnimalTestCase(TestCase):
    def setUp(self):
        Animal.objects.create(name="lion", sound="roar")
        Animal.objects.create(name="cat", sound="meow")

    def test_animals_can_speak(self):
        """Animals that can speak are correctly identified"""
        lion = Animal.objects.get(name="lion")
        cat = Animal.objects.get(name="cat")
        self.assertEqual(lion.speak(), 'The lion says "roar"')
        self.assertEqual(cat.speak(), 'The cat says "meow"')
```

<!-- v -->

## Lancer les tests

```bash
./manage.py test
```

Seulement pour un module

```bash
./manage.py test manimals
```

Ou une classe, ou une méthode

```bash
./manage.py test manimals.tests.AnimalTestCase.test_animals_can_speak
```
<!-- v -->

## Arborescence conseillée

Créer dans un dossier ``tests``, un fichier de tests
(``test_views.py``, ``test_models.py``, ...) par fichier de
l'application (``views.py``, ``models.py``, ...).

```plaintext
├── todolist
│   ├── __init__.py
│   ├── forms.py
│   ├── models.py
│   ├── views.py
│   ├── tests
│   │   ├── __init__.py
│   │   ├── test_forms.py
│   │   ├── test_models.py
│   │   └── test_views.py
```

<!-- s -->

# TP : mes premiers tests

<!-- v -->

Récupérer le code du TP

<!-- v -->

## Exécution

Dans le dossier du projet

```console
$ ./manage.py test
System check identified no issues (0 silenced).

Ran 0 tests in 0.000s

OK
```

<!-- v -->

## Premier test (qui plante)

```python
# poll/tests.py
from django.test import TestCase


class TestPoll(TestCase):
    def test_truth(self):
        assert False
```

<!-- v -->

### Résultat


```console
Creating test database for alias 'default'...
System check identified no issues (0 silenced).
F
======================================================================
FAIL: test_truth (polls.tests.TestPoll)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "/home/numahell/Dev/formation/django/tp-django/monprojet/polls/tests.py", line 6, in test_truth
    assert False
AssertionError

----------------------------------------------------------------------
Ran 1 test in 0.001s

FAILED (failures=1)
Destroying test database for alias 'default'...
```

<!-- v -->

## Premier test (qui passe)


```python
class TestPoll(TestCase):
    def test_truth(self):
        assert True
```

<!-- v -->

### Résultat

```console
Creating test database for alias 'default'...
System check identified no issues (0 silenced).
.
----------------------------------------------------------------------
Ran 1 test in 0.001s

OK
Destroying test database for alias 'default'...
```

<!-- s -->

# Tester chaque partie de l'application

<!-- v -->

## Testons les URL

```python
from django.test import TestCase


class TestPoll(TestCase):
    def test_resolve_home_url(self):
        response = self.client.get('/polls/')
        self.assertEqual(response.status_code, 200)
```

`./manage.py test` donnera le même résultat que précédemment

<!-- v -->

## Testons le contenu

```python
from .models import Question, Poll


class TestPoll(TestCase):
    def setUp(self):
        self.question1 = Question.objects.create(question_text='On fait une pause ?')

    def test_resolve_home_url(self):
        response = self.client.get('/polls/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'On fait une pause ?')
```

<!-- v -->

## A savoir

La classe `dango.test.TestCase` :
- reset la base de données à l'état initial à chaque fin de test
- possède l'attribut `client` qui permet de faire des requêtes et conserver
les cookies de session
    - `self.client.get()` et `self.client.post()`
    - `self.client.login()` et `self.client.force_login()`

<!-- v -->

- permet de créer des données de test pour toutes les méthodes en
surchargeant `setUp()` ou `setUpTestData()`
- permet de charger des fixtures (données initiales en début de test)
- fournit quantités d'[assertions](https://docs.djangoproject.com/en/3.0/topics/testing/tools/#assertions)
    - `self.assertEqual(1, 3 - 2)`
    - `self.assertContains(response, "Mon titre")`

<!-- s -->

# TP : écrire les tests pour la vue détail
