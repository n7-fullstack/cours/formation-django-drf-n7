---
title: Pièges à éviter en Python
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---

# Pièges à éviter

<!-- v -->

## Paramètre par défaut modifiable

```python
>>> def fonc(things=[]):
...     things.append(1)
...     return things
...
>>> fonc()
[1]
>>> fonc()
[1, 1]
>>> fonc()
[1, 1, 1]
```

### Explication

Les paramètres par défaut sont associés à l'objet fonction quand il est créé :

```python
>>> fonc.__defaults__
([1, 1, 1],)
```


<!-- v -->

### Paramètre par défaut modifiable : contournement

```python
>>> def fonc(things=None):
...     if things is None:
...         things = []
...     things.append(1)
...     return things
...
>>> fonc()
[1]
>>> fonc()
[1]
>>> fonc()
[1]
```

Voir la FAQ : [Why are default values shared between objects?](https://docs.python.org/3/faq/programming.html#why-are-default-values-shared-between-objects)

<!-- v -->

## Portée des variables

```python
>>> fonctions = []
>>> for n in range(5):
...     def f():
...         return n
...     fonctions.append(f)
...
>>> [f() for f in fonctions]
[4, 4, 4, 4, 4]
```

### Explication

La variable globale `n` est écrasée à chaque tour de boucle.

<!-- v -->

### Portée des variables : contournement

```python
>>> fonctions = []
>>> for n in range(5):
...     def f(n=n):
...         return n
...     fonctions.append(f)
...
>>> [f() for f in fonctions]
[0, 1, 2, 3, 4]
```

<!-- v -->

### Explication

La valeur par défaut est associée à la fonction au moment où celle-ci est créée.

Voir la FAQ : [Why do lambdas defined in a loop with different values all return the same result?](https://docs.python.org/3/faq/programming.html#why-do-lambdas-defined-in-a-loop-with-different-values-all-return-the-same-result)

<!-- v -->

## Attention aux calculs avec des flottants

```python
>>> 0.1 + 0.2
0.30000000000000004
>>> 0.1 + 0.2 == 0.3
False
```

### Explication

Les nombres flottants ne sont [pas complètement précis](https://docs.python.org/3.6/tutorial/floatingpoint.html).

<!-- v -->

### Solution

Utiliser le type [Decimal](https://docs.python.org/3/library/decimal.html#decimal-objects) :

```python
>>> from decimal import Decimal
>>> Decimal('0.1') + Decimal('0.2') == Decimal('0.3')
True
```

<!-- s -->

# Idiomes

<!-- v -->

## Itérer avec l'index

**Ne pas faire :**

```python
>>> i = 0
>>> for obj in ["one", "two", "three"]:
...     print("{0} => {1}".format(i, obj))
...     i += 1
...
0 => one
1 => two
2 => three
```

**Mais faire :**

```python
>>> for i, obj in enumerate(["one", "two", "three"]):
...     print("{0} => {1}".format(i, obj))
...
0 => one
1 => two
2 => three
```

<!-- v -->

## Ne pas écrire de getters / setters

**Ne pas faire :**

```python
>>> class Foo:
...     def __init__(self, val):
...         self._attr = val
...     def set_attr(self, val):
...         self._attr = val
...     def get_attr(self):
...         return self._attr
...
>>> obj = Foo(42)
>>> obj.get_attr()
42
>>> obj.set_attr(43)
>>> obj.get_attr()
43
```

<!-- v -->

**Faire :**

```python
>>> class Foo:
...     def __init__(self, val):
...         self.attr = val
>>> obj = Foo(42)
>>> obj.attr
42
>>> obj.attr = 43
>>> obj.attr
43
```

*Oui mais si on veut intercepter les accès par la suite ?*

<!-- v -->

## Utiliser le décorateur `@property`

```python
>>> class Foo:
...     def __init__(self, val):
...         self._attr = val
...     @property
...     def attr(self):
...         return self._attr * 2
...     @attr.setter
...     def attr(self, val):
...         self._attr = val + 1
>>> obj = Foo(42)
>>> obj.attr
84
>>> obj.attr = 43
>>> obj.attr
88
```
