---
title: Rappels en Python
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---

# Rappels en Python

<!-- v -->

## Sommaire

* Listes, tuples et dictionnaires
* Fonctions et passage d'arguments
* Packages et modules
* POO: héritage multiple et appel de méthode parente

<!-- s -->

# Listes, tuples et dictionnaires

<!-- v -->

## Les listes

```python
>>> liste_vide = []
>>> ma_liste = [1, "deux", 3]
>>> len(ma_liste)
3
```

<!-- v -->

### Concaténation

```python
>>> ma_liste = ma_liste + [5, 6]
>>> ma_liste
[1, 'deux', 3, 5, 6]
```

### Notation indicaire

```python
>>> ma_liste[1:-1]
['deux', 3, 5]
```

<!-- v -->

### Modification

```python
>>> ma_liste[1] = 2
>>> ma_liste
[1, 2, 3, 5, 6]
```

Méthodes de modification

```python
>>> ma_liste.append(4)
>>> ma_liste
[1, 'deux', 3, 4]
>>> ma_liste
[1, 'deux', 3, 4, 5, 6]
>>> ma_liste.pop(0)
1
>>> ma_liste
['deux', 3, 4, 5, 6]
```

Les listes peuvent être modifiées.

<!-- v -->

## Les tuples

### Création

```python
>>> mon_tuple = ("un", 1)
>>> len(mon_tuple)
2
```

### Notation indicaire

```python
>>> mon_tuple[-1]
1
>>> mon_tuple[0]
'un'
```

<!-- v -->

### Concaténation

```python
>>> mon_tuple = mon_tuple + (1.0, )
>>> mon_tuple
('un', 1, 1.0)
```

### Modificaiton ?

```
>>> mon_tuple[0] = "deux"
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: 'tuple' object does not support item assignment
```

Les tuples ne peuvent pas être modifiés.

<!-- v -->

## Listes ou tuples ?

En général, on réserve les tuples pour :

* collections relativement **petites** (moins de 10 éléments)
* données de type **enregistrement**, souvent de types **variés**
* regroupement de **constantes** (enum)

<!-- v -->

Les listes :

* **grand nombre** d'éléments
* éléments de **type homogène**
* contenu de la collection doit être **modifiable**
* souvent utilisées dans des **boucles**

<!-- v -->

## Listes *et* tuples

On les utilise souvent ensemble :

```python
>>> collection = [("one", 1), ("two", 2), ("three", 3)]
>>> type(collection)
<class 'list'>
>>> type(collection[1])
<class 'tuple'>
```

<!-- v -->

## Dictionnaires

```python
cities = {
  "france": "paris",
  "england": "london",
  "germany": "berlin"
}
```

<!-- v -->

### Opérations de base

Récupérer un élément par sa clé

```python
>>> cities["france"]
'paris'
```

Ajout d'un élément

```python
>>> cities["belgium"] = "brussels"
```

<!-- v -->

### Clés manquantes

```python
>>> cities["wonderland"]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
KeyError: 'wonderland'
```

La méthode `get`

```python
>>> city = cities.get("wonderland")
>>> type(city)
<class 'NoneType'>
>>> cities.get("wonderland", "nowhere")
'nowhere'
```

<!-- v -->

### Accéder aux clés et aux valeurs

```python
>>> cities.keys()
dict_keys(['france', 'england', 'germany', 'belgium'])
>>> cities.values()
dict_values(['paris', 'london', 'berlin', 'brussels'])
>>> cities.items()
dict_items([('france', 'paris'), ('england', 'london'), ('germany', 'berlin'), ('belgium', 'brussels')])
```

### Le constructeur `dict`

```python
>>> dict([("spain", "madrid"), ("italy", "rome")])
{'spain': 'madrid', 'italy': 'rome'}
```

<!-- v -->

## Itération

### Boucle `for`

```python
>>> for key, value in cities.items():
...     print("{0} => {1}".format(key, value))
france => paris
england => london
germany => berlin
belgium => brussels
```

<!-- v -->

### Liste en compréhension

```python
>>> [cities[key].capitalize() for key in cities]
['Paris', 'London', 'Berlin', 'Brussels']
```

<!-- s -->

# Fonctions

<!-- v -->

## Définition d'une fonction

```python
def substract(a, b):
    return a - b
```

## Appel d'une fonction

```python
>>> substract(2, 3)
-1
```

<!-- v -->

## Nommer les paramètres

```python
>>> substract(b=2, a=3)
1
```

## Valeurs par défaut

```python
>>> def substract(a, b=1):
...     return a - b
...
>>> substract(2)
1
>>> substract(2, 2)
0
```

<!-- v -->

## Paramètres variables

**Positionnels**

```python
>>> def multi_args(*args):
...     print(len(args))
...
>>> multi_args(1, 2, "trois")
3
```

**Nommés**

```python
>>> def multi_named_args(**kwargs):
...     for k, v in kwargs.items():
...         print(f"{k} => {v}")
...
>>> multi_named_args(firstname="Alice", lastname="Wonderland", age=14)
firstname => Alice
lastname => Wonderland
age => 14
```

<!-- v -->

Ça fonctionne aussi à l'appel. Soit :

```python
>>> def greet(first_name, last_name):
...     print(f"Hello {first_name} {last_name}")
...
```

**Positionnels**

```python
>>> myargs = ["Alice", "Wonderland"]
>>> greet(*myargs)
Hello Alice Wonderland
```

**Nommés**

```python
>>> mykwargs = {"last_name": "Cooper", "first_name": "Alice"}
>>> greet(**mykwargs)
Hello Alice Cooper
```


<!-- v -->

## Exemple complet

```python
>>> def print_params(a, *args, **kwargs):
...     print(type(a), a)
...     print(type(args), args)
...     print(type(kwargs), kwargs)
...
>>> print_params("Bonjour", "Alice", "Bob", france="Paris", england="London")
<class 'str'> Bonjour
<class 'tuple'> ('Alice', 'Bob')
<class 'dict'> {'france': 'Paris', 'england': 'London'}
```


<!-- s -->

# Packages et modules

<!-- v -->

## Module

Un fichier `.py` qui contient du code Python.

## Package

Un répertoire qui contient des modules et des packages.

Un package peut contenir un module `__init__.py`, exécuté lorsqu'on importe le package.

<!-- v -->

## Création d'un package

```console
mypackage/
├── __init__.py
└── mymodule.py
```

Contenu du fichier `mypackage/__init__.py`

```python
package_value = 23
```

Contenu du fichier `mypackage/mymodule.py`

```python
def foo():
    print("Je suis " + __name__)
```

<!-- v -->

## Utilisation d'un package

```python
>>> import mypackage
>>> mypackage.package_value
23
>>> mypackage.mymodule.foo()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: module 'mypackage' has no attribute 'mymodule'
>>> import mypackage.mymodule
>>> mypackage.mymodule.foo()
Je suis mypackage.mymodule
>>> from mypackage import mymodule
>>> mymodule.foo()
Je suis mypackage.mymodule
```

<!-- v -->

## À retenir

Les modules et sous-packages contenus dans un package ne sont pas automatiquement importés.

<!-- s -->

# Programmation objet

<!-- v -->

## Héritage et attributs de classe

```python
>>> class Parent:
...     attr = 42  # Attribut de classe
...
```

`Child` hérite de la classe `Parent`

```python
>>> class Child(Parent):  # Héritage
...     pass
...
```

<!-- v -->

Accéder à l'attribut de la classe

```python
>>> Child.attr
42
```

Instancier la classe = créer un objet à partir de la classe

```python
>>> obj = Child()  # Instanciation
>>> obj.attr
42
```

<!-- v -->

## Attributs d'instance

```python
>>> obj.attr = 23  # Attribut d'instance
>>> obj.attr       # masque l'attribut de classe
23
>>> Child.attr     # Attribut de classe inchangé
42
>>> obj2 = Child()
>>> obj2.attr      # Pas d'attribut d'instance
42
>>> del obj.attr   # Suppression de l'attribut d'instance
>>> obj.attr       # On accède de nouveau à l'attribut de classe
42
```

<!-- v -->

### À retenir

Les attributs sont recherchés d'abord sur l'instance,
puis sur la classe.

<!-- v -->

## Héritage multiple

```python
>>> class Dad:
...    name = "Dupont"
...
>>> class Mom:
...    name = "Durand"
...
>>> class Child(Dad, Mom):
...    pass
...
>>> child = Child()
>>> child.name
'Dupont'
```

<!-- v -->

### À retenir

On parcourt les classes parentes de gauche à droite jusqu'à trouver l'attribut 
recherché (MRO: _Method resolution order_).

<!-- v -->

## Initialiseur et surcharge de méthode

```python
>>> class Dad:
...    name = "Dupont"
...    def __init__(self, sport):
...        self.sport = sport
...    def greeting(self):
...        return "Bonjour, j'aime le " + self.sport
...
>>> class Child(Dad):
...    def __init__(self):
...        pass
...    def greeting(self):
...        return "Bonjour, j'aime coder en Python"
...
>>> dad = Dad("foot")
>>> dad.greeting()
"Bonjour, j'aime le foot"
>>> child = Child()
>>> child.greeting()
"Bonjour, j'aime coder en Python"
>>> child.name
'Dupont'
```


<!-- v -->

## Appel à la méthode parente

```python
>>> class Dad:
...    def __init__(self, sport):
...        self.sport = sport
...    def greeting(self):
...        return "Bonjour, j'aime le " + self.sport
...
>>> class Child(Dad):
...    def greeting(self):
...        return super().greeting() + " et coder en Python"
...
>>> child = Child("rugby")
>>> child.greeting()
"Bonjour, j'aime le rugby et coder en Python"
```

## À retenir

Utiliser `super` pour accéder à un attribut parent.

<!-- s -->

# Aller plus loin

* [The Python Tutorial](https://docs.python.org/3/tutorial/index.html)
* [The Python Standard Library](https://docs.python.org/3/library/index.html)
* [Notions de Python avancées](https://zestedesavoir.com/tutoriels/954/notions-de-python-avancees/)

<!-- v -->

Aller vers [Django initiation](./01-django-initiation.md)
