---
title: Introduction
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---

# Django / Django REST Framework

## 4 jours

<!-- v -->

## Infos pratiques

- matin 9h-12h30
- après-midi 13h30-17h
- 1 pause 15 minutes matin et après-midi

email : emmanuelle.helly@makina-corpus.com

<!-- v -->

## Sommaire

<!-- v -->

### Les concepts de Django

- Projet, applications
- Requêtes et réponses
- Vues, templates, URLs
- Models, champs et ORM
- Vues et formulaires pour les models
- Models et relations

<!-- v -->

### Les outils de Django

- Les outils de debug
- Écrire des tests

<!-- v -->

### REST

- Méthodes HTTP
- Principes des APIs REST

<!-- v -->

### Django REST Framework

- Ajouter DRF au projet
- Première API
- Serializers
- Viewsets et Routers
- Documenter son API
- Relations
- Authentifications
- Versions API, pagination

<!-- v -->

## TP pour le tutoriel

Partage du code via VS Code Live (si ça fonctionne)

Application de sondage 

→ Tutoriel Django officiel

Autre idée ?

<!-- s -->

Aller vers [Django initiation](./01-django-initiation.md)
