---
title: Django intermédiaire
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---


# Django Intermédiaire 

<!-- v -->

## Sommaire

- Vues basées sur les classes
- Requêtes et réponses
- Vues liste / détail en json
- Formulaires (à venir)
- Vue écriture en json

<!-- v -->

## TP

- Finaliser les vues liste et détail et leurs templates associés
- Écrire les tests pour la vue page d'accueil, la vue liste et la vue détail
- Créer une vue liste qui renvoie du json sur "/api/v0"
- Transformer les vues fonctions en vues basées sur les classes : vérifier grâce aux tests que l'appli fonctionne toujours
- (Bonus) Créer une vue liste qui renvoie du json en utilisant DRF

<!-- s -->

# Vues basées sur les classes

<!-- v -->

## Class-based views

Une vue *basée sur une classe* est une classe qui prend en entrée une **requête HTTP**
et retourne une **réponse HTTP** à travers sa méthode  `dispatch()` qui appelle la
méthode `get` ou `post()` selon le verbe HTTP.

<!-- v -->

### Un exemple tiré de la documentation Django

```python
# some_app/views.py
from django.http import HttpResponse
from django.views.generic import View

class CurrentDatetimeView(View):
    def get(self, request, *args, **kwargs):
        now = datetime.datetime.now()
        html = "<html><body>It is now %s.</body></html>" % now
        return HttpResponse(html)
```
  			
<!-- v -->

Le chemin de l'URL avec CBV aura cette forme :

```python
# some_app/urls.py
from django.urls import path
from some_app.views import CurrentDatetimeView
urlpatterns = [
    path('date-courante', CurrentDatetimeView.as_view()),
]
```

<!-- v -->

## Quelques classes de base

* ``View`` : classe *mère* et fournit le workflow vu précédemment
* ``TemplateView`` : faire le rendu d'un template avec un contexte
* ``FormView`` : gestion d'un formulaire en permettant une bonne organisation du code

<!-- v -->

### Afficher des instances

* ``ListView`` permet de lister très simplement des instances d'un modèle.
* ``DetailView`` permet d'afficher le détail d'une instance d'un modèle.

<!-- v -->

### Modifier des instances

* ``CreateView`` et ``UpdateView`` sont très utiles pour la création/modification
d'instance, de l'affichage du formulaire jusqu'à l'enregistrement.
* ``DeleteView`` facilite l'implémentation de vues pour la suppression d'instance.

Site pour un aperçu : <http://ccbv.co.uk/>

<!-- v -->

## Transformer la vue liste en vue de type classe

On convertit notre [vue en classe](https://docs.djangoproject.com/fr/1.11/topics/class-based-views/) :

```python
# polls/views.py
class IndexView(View):
    """Vue index qui renvoie la liste des questions"""

    def get(self, request):
        question_list = Question.objects.all()
        context = {
            'latest_question_list': question_list
        }

        return render(request, 'index.html', context)
```

<!-- v -->

Mise à jour de la configuration des URLs :

```python
# polls/urls.py
    path('', views.IndexView.as_view(), name="home"),
```

Lancez les tests pour vérifier si rien n'est cassé.

<!-- s -->

# TP : Vues basées sur des classes

Transformer les vues fonctions en vues basées sur les classes : vérifier grâce aux tests que l'appli fonctionne toujours

<!-- s -->

# Requête et réponses

<!-- v -->

## Une vue : reçoit une requête et renvoie une réponse

Une **vue** est une fonction qui prend un objet `HttpRequest` et renvoie un objet `HttpResponse`.

<!-- v -->

## Déroulement d'une requête HTTP

Quand Django reçoit une requête HTTP :

* Crée l'objet `HttpRequest` correspondant à la requête du client
* Cherche la fonction de vue associée à l'URL
* Appelle cette fonction en lui passant l'objet `HttpRequest` en paramètre
* Récupère un objet `HttpResponse` en retour de la fonction ou de la classe
* Répond au client

<!-- v -->

## L'objet `HttpRequest`

Permet d'accéder à de nombreux attributs tels que

* Le schéma (ex. `http` ou `https`), le domaine, et le chemin formant l'URL
* La méthode (ex. `GET`, `POST`, `PUT`, `DELETE`)
* Les headers HTTP (ex. `Content-Type`)
* Les paramètres et les fichiers uploadés
* Les cookies

<!-- v -->

Peut être lu comme un fichier/flux

* `request.read()`
* `request.readline()`
* `for line in request:`

_cf._ [documentation objet `HttpRequest`](https://docs.djangoproject.com/fr/stable/ref/request-response/#httprequest-objects)

<!-- v -->

## L'objet `HttpResponse`

Permet de régler de nombreux attributs tels que

* Le statut HTTP (ex. `200 OK`, `404 Not Found`)
* Le contenu de la réponse (ex. du code HTML, des données sérialisées en JSON)
* Les headers HTTP (ex. `Content-Type`)
* Les cookies

<!-- v -->

Peut être instancié directement avec le contenu comme paramètre

```python
response = HttpResponse("foobar")
```

Peut être écrit comme un flux

```python
request.write()
```

Est dérivé en sous-classes (ex. `HttpResponseRedirect`)

_cf._ [documentation objet `HttpResponse`](https://docs.djangoproject.com/fr/stable/ref/request-response/#httpresponse-objects)

<!-- v -->

## Sous-classe JsonResponse

- Permet la création d'une réponse encodée en `Json`
- Prend un dictionnaire en paramètre

On va l'expérimenter par la suite

_cf._ [documentation objet `JsonResponse`](https://docs.djangoproject.com/fr/3.1/ref/request-response/#httpresponse-objects)

<!-- s -->

# Formulaires

<!-- v -->

## En HTML

- dans les balises `<form>...</form> `
- champs texte, select, radio button…
- champ `<input type="submit" value="Valider" />`
- URL où les données doivent être envoyées

<!-- v -->

### Méthode d'envoi des données

`GET` ou `POST`

- `GET` envoyer les données via l'URL (ex : formulaire de recherche)
- `POST` envoyer les données dans le `POST` de la requête (tout autre formulaire)


<!-- v -->

## La bibliothèque `django.forms`

Bibliothèque de gestion de formulaires

* `Widget` : rendu d'un widget HTML (ex: un champ `<input>`, `<textarea>`, ...)
* `Field` : initialisation et validation d'un champ de formulaire
* `Form` : gérer un ensemble de champs de formulaire, 
  l'initialisation, le rendu et la validation du formulaire global

<!-- v -->

### Formulaires basés sur un modèle

* La classe `ModelForm` : permet de gérer des formulaires basés sur des modèles
(création / modification d'une instance du modèle)

<!-- v -->

## Déclaration d'un formulaire simple

### Formulaire de contact

```python
# contact/forms.py
from django import forms

class ContactForm(forms.Form):
    subject = forms.CharField(max_length=100)
    message = forms.CharField()
    sender = forms.EmailField()
    cc_myself = forms.BooleanField(required=False)
```

<!-- v -->

### Quelques méthodes souvent utilisées

* `__init__` : personnaliser l'initialisation du formulaire
  (ex : pré-remplir le champ `sender` par l'email de l'utilisateur connecté)
* `clean` : personnaliser la validation du formulaire
  (ex : vérifier que `sender` a bien été fourni si `cc_myself` a été coché)

<!-- v -->

## Quelques champs de formulaire

Les principaux

* texte : `CharField`, `EmailField`,…
* nombres : `FloatField`, `IntegerField`
* booléens : `BooleanField`
* sélection : `ChoiceField`, `MultipleChoiceField`
* dates : `DateField`, `DateTimeField`,…
* fichiers : `FileField`, `ImageField`,…
* relation : `ModelChoiceField`, `ModelMultipleChoiceField`

_cf_ <https://docs.djangoproject.com/fr/stable/ref/forms/fields/>

<!-- v -->

## Champs et Widgets

Chaque champ peut être initialisé avec des options, par ex
`label`, `required`, `initial` et `widget`.

Les champs de formulaire gèrent les données, les widgets gèrent la manière
dont elles sont saisies.

Par exemple, un `CharField` gérera des données de texte, et aura par défaut
un widget `TextInput`, mais il est possible de spécifier un widget `Textarea`.

```python
class CommentForm(forms.Form):
    title = forms.CharField(label="Donner un titre à votre commentaire")
    message = forms.CharField(widget=forms.Textarea)
```

<!-- v -->

## Formulaire dans une *Function-based view*

Formulaire de contact

```python
from django.shortcuts import render, redirect
from myapp.forms import ContactForm

def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)  # instanciation avec données
        if form.is_valid():
            # Traite les données dans form.cleaned_data puis redirige
            ...
            return redirect('/thanks/')
    else:
        form = ContactForm()  # instanciation sans données
    # Affichage du formulaire via un template
    return render(request, 'contact.html', {'form': form})
```

<!-- v -->

### Méthode simplifiée

```python
def contact(request):
    form = ContactForm(request.POST or None)
    if form.is_valid():
        ...
        return redirect('/thanks/')
    return render(request, 'contact.html', {'form': form})
```

<!-- v -->

## Rendu dans un template

```django
<form action="" method="post">
  {% csrf_token %}
  {{ form.as_p }}
  <input type="submit" value="Submit" />
</form>
```

`{% csrf_token %}` : pour protéger 
le formulaire des attaques de type CSRF (*Cross Site Request Forgeries*).

<!-- v -->

### Différents rendus

* `as_p`: chaque champ est rendu dans un paragraphe
* `as_ul`: chaque champ est rendu dans une ligne de liste
* `as_table`: chaque champ est rendu dans une ligne de tableau

Des packages de la communauté _cripsy_forms_ permet
d'afficher le bon markup pour un framework HTML/CSS (Bootstrap, Foundation, Uniform, …)

<!-- s -->

# Formulaires et modèles

<!-- v -->

## Les formulaires de modèles `ModelForm`

Créer automatiquement des formulaires basés sur des modèles.

* Classe `Meta` est nécessaire pour préciser sur quel modèle doit se baser le formulaire
* La méthode `__init__` prend en argument l'instance du modèle à modifier (ou `None` dans le cas d'une création)
* Méthode `save` : enregistrer l'instance éditée via le formulaire
 
<!-- v -->

* Champs automatiquement listés s'ils n'ont pas la propriété `editable=False`
* Possible d'ajouter d'autres champs

<!-- v -->

### Exemple

En reprenant le modèle Task :

```python
### todolist/models.py
class Task(models.Model):
    title = models.CharField(max_length=100)
    deadline = models.DateField()
    done = models.BooleanField(default=False)
```

Le formulaire basé sur `ModelForm`

```python
### todolist/forms.py
class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ('title', 'deadline')
        #fields = '__all__'
```

<!-- v -->

### Avec une Function-based view

```python
### todolist/views.py
def add_task(request):
    form = TaskForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('/tasks/')
    return render(request, 'add_task.html', {'form': form})
```

**Note** : pour modifier une instance, il faut la passer en paramètre du formulaire

```python
task = Task.objects.get(pk=pk)
form = TaskForm(instance=task)
```

<!-- v -->

## Utilisation de ModelForm avec une Class-Based View

```python
class AddTaskView(CreateView):
    form_class = TaskForm
    success_url = reverse_lazy('task:list')
    template_name = 'add_task.html'
```

ou même plus simple, puisque le formulaire est facultatif

```python
class AddTaskView(CreateView):
    model = Task
    fields = ('title', 'release')
    success_url = reverse_lazy('task:list')
    template_name = 'add_task.html'
```

<!-- v -->

## Déclarer le formulaire

```python
# todolist/forms.py
class TaskForm(forms.ModelForm):

    class Meta:
        model = Task
        fields = ("title", "deadline", "authors")
```

<!-- v -->

## La vue formulaire

```python
# todolist/views.py
class TaskCreateView(CreateView):
    """Task create view"""
    form_class = TaskForm
    template_name = 'todolist/task_form.html'
    success_url = '/task/list'
```

<!-- v -->

## Le template pour l'ajout

```html
<!-- teaching/templates/todolist/task_form.html -->
<form method="post">
    {% csrf_token %}
    {{ form.as_p }}
    <input type="submit" value="Submit" />
</form>
```

<!-- v -->

## L'URL pattern

```python
# todolist/urls.py
path('task/add', views.TaskCreateView.as_view(), name="task-create"),
```
