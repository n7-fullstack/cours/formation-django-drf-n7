---
title: Django Rest Framework Initiation
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---

# Django Rest Framework<br>Initiation

<!-- v -->

## Sommaire

- Notions sur REST
- Premiers pas avec DRF
- Serializers
- Requêtes et réponses en DRF
- Les vues
- Relations imbriquées
- Viewsets & routers
- Filtrage

<!-- s -->

# Concepts API REST

<!-- v -->

## HTTP

* Protocole du web
* Sans état : requête -> réponse (en général)
* Méthode (ou verbes) : GET, POST, PUT, DELETE, etc.
* Headers : Accept, Accept-Language, Content-Type, etc.
* Code de retour (status code) : 20x, 30x, 40x, 50x, [etc.](http://httpstatuses.com)

<!-- v -->

## JSON

* Sous-ensemble de la syntaxe de définition d'objects de JavaScript ([json.org](http://json.org/))
* Types simples : nombres, chaines, booleens, null
* Types composés : tableaux et objets (équivalents aux listes et dictionnaires de Python)
* Notamment pas de type date, on utilise généralement des chaines au format <a href="https://en.wikipedia.org/wiki/ISO_8601#Combined_date_and_time_representations">iso8601</a>

<!-- v -->

```python
>>> import json
>>> python_data = {'languages': ["Python", "Java"], 'year': 2017}
>>> json_string = json.dumps(python_data)
>>> json_string
'{"languages": ["Python", "Java"], "year": 2017}'
>>> python_data_2 = json.loads(json_string)
>>> python_data_2["languages"][0]
u'Python'
```

<!-- v -->

## Requête

```console
$ curl -v http://makina-corpus.com/
> GET / HTTP/1.1
> Host: makina-corpus.com
> User-Agent: curl/7.54.0
> Accept: */*
>
< HTTP/1.1 301 Moved Permanently
< Server: nginx
< Date: Tue, 30 May 2019 14:52:48 GMT
< Content-Type: text/html
< Content-Length: 178
< Location: https://makina-corpus.com/
<
<html>
<head><title>301 Moved Permanently</title></head>
<body bgcolor="white">
<center><h1>301 Moved Permanently</h1></center>
<hr><center>nginx</center>
</body>
</html>
```

<!-- v -->

## Notions sur REST

<!-- v -->

### Signification de l'acronyme

**RE**presentational **S**tate **T**ransfer

### Définition informelle

> Une manière de concevoir des APIs web en se basant sur la sémantique du web.

<!-- v -->

### Caractéristiques pratiques

* Utiliser HTTP pour communiquer entre processus
* Ressources et représentations
* Ressource identifiée par une URL
* JSON souvent utilisé pour les représentations
* Lier les ressources entre elles avec des URLs dans les représentations

<!-- v -->

### Principales [méthodes HTTP](https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html)

<table>
<tr><th>Méthode</th><th>Utilisation</th><th>Code de succès</th></tr>
<tr><td>GET</td><td>obtenir une représentation de la ressource</td><td>200</td></tr>
<tr><td>POST</td><td>créer une ressource dans une collection, le serveur renvoie la nouvelle URL</td><td>201</td></tr>
<tr><td>PUT</td><td>créer ou écraser une ressource à une URL donnée</td><td>201, 200, 204</td></tr>
<tr><td>DELETE</td><td>supprimer une ressource</td><td>200, 204</td></tr>
</table>

<!-- s -->

# Premiers pas avec Django REST Framework

<!-- v -->

## Mise en place

**requirements.txt**

```console
pip install djangorestframework
```

**settings.py**

```python
INSTALLED_APPS = (
    ...
    'rest_framework',
)
```

<!-- v -->

## Premiers pas

Une approche par le haut

### Serializer

```python
# todolist/serializers.py
from rest_framework import serializers

class TaskSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Task
        fields = ('title')
```

<!-- v -->

## View associée

```python
# todolist/views.py
from rest_framework import viewsets

class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all().order_by("title")
    serializer_class = TaskSerializer
```

<!-- v -->

## Configuration des URLs

```python
# todolist/urls.py
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'todolist', views.TaskViewSet)

urlpatterns = [
    # ...
    path('api/v1/', include(router.urls)),
]
```

<!-- v -->

## Tester

Exécuter les tests automatisés.

Tester avec curl :

```console
$ curl -v http://localhost:8000/api/v1/tasks/
```

Visiter [http://localhost:8000/api/v1/](http://localhost:8000/api/v1/) avec son navigateur.

<!-- v -->

## Pagination

```python
# bistrot_project/settings.py
REST_FRAMEWORK = {
  'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
  'PAGE_SIZE': 5
}
```

<!-- v -->

# TP : Utiliser DRF

Créer une vue liste et détail pour les auteurs,
qui répond à l'url `/api/v1/`

<!-- s -->

# Vocabulaire

<!-- v -->

- Parsers et renderers
- Sérialiseurs
- Requête et réponse
- Vues, Vues génériques, Viewsets
- Routeurs

<!-- s -->

# Les serializers

<!-- v -->

## Sérialisation

* les [**serializers**](http://www.django-rest-framework.org/api-guide/serializers/) convertissent des instances de modèles et de querysets en structures de données Python natives
* ces structures de données Python peuvent ensuite être converties en JSON, XML, etc. par des [**renderers**](http://www.django-rest-framework.org/api-guide/renderers/)

![Sérialisation en JSON](img/serializers1.png)

<!-- v -->

## Désérialisation

* les [**parsers**](http://www.django-rest-framework.org/api-guide/parsers/) convertissent les formats textuels en structures Python
* les [**serializers**](http://www.django-rest-framework.org/api-guide/serializers/) créent des instances de modèles à partir de ces structures

![Désérialisation depuis JSON](img/serializers2.png)

<!-- v -->

## Exemple : lister les tâches

Pour bien comprendre les serializer, nous utiliserons un serializer simple.

### Le serializer

```python
# conso/serializers.py
from todolist.models import Task
from rest_framework import serializers

class TaskSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=150)
    deadline = serializers.DateField()
```

<!-- v -->

### Utilisation d'un serializer**

```python
>>> from todolist.models import Task
>>> task1 = Task.objects.first()
>>> from todolist.serializers import TaskSerializer
>>> serializer = TaskSerializer(task1)
>>> serializer.data
{'title': 'Réparer la fenêtre', 'deadline': '2021-01-08T10:35:29.549014Z'}
```

Rendu au format JSon

```python
>>> JSONRenderer().render(serializer.data)
b'{"title":"Réparer la fenêtre","deadline":"2021-01-08T10:35:29.549014Z"}'
```

<!-- v -->

### Le serializer dans la vue

```python
def api_tasks(request):
    """API Polls get and post"""
    if request.method == 'GET':
        task_list = Task.objects.all()
        serializer = TaskSerializer(task_list, many=True)
        response_data = {"results": serializer.data}
        return JsonResponse(response_data)
```

<!-- v -->

## Désérialisation : création d'une tâche 

```python
>>> from todolist.models import Task
>>> from todolist.serializers import TaskSerializer
>>> from rest_framework.parsers import JSONParser
```

<!-- v -->

### Les données d'entrée

```python
>>> content = b'{"title":"Faire des courses","deadline":"2021-01-28T10:35:29.549014Z"}'
>>> import io
>>> stream = io.BytesIO(content)
>>> data = JSONParser().parse(stream)
```

Remarque : en essayant sans la date, on a une erreur.
Pour permettre de ne pas renseigner la date de publication

```python
deadline = serializers.DateField(required=False)
```
<!-- v -->

### Activation du serializer

```python
>>> serializer = TaskSerializer(data=data)
>>> serializer.is_valid()
True
>>> serializer.save()
Traceback (most recent call last):
  File "<console>", line 1, in <module>
  File "/home/numahell/Dev/formation/django/tp-django/myvenv/lib/python3.8/site-packages/rest_framework/serializers.py", line 205, in save
    self.instance = self.create(validated_data)
  File "/home/numahell/Dev/formation/django/tp-django/myvenv/lib/python3.8/site-packages/rest_framework/serializers.py", line 170, in create
    raise NotImplementedError('`create()` must be implemented.')
NotImplementedError: `create()` must be implemented.
>>> 
```

Nous devons implémenter la création

<!-- v -->

### La méthode de création

```python
class TaskSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=200)
    deadline = serializers.DateTimeField(required=False)

    def create(self, validated_data):
        return Task.objects.create(**validated_data)
```

<!-- v -->

```python
>>> from todolist.models import Task
>>> from todolist.serializers import TaskSerializer
>>> from rest_framework.parsers import JSONParser
>>> import io
>>> content = b'{"title":"Faire des courses","deadline":"2021-01-28T10:35:29.549014Z"}'
>>> stream = io.BytesIO(content)
>>> data = JSONParser().parse(stream)
>>> serializer = TaskSerializer(data=data)
>>> serializer.is_valid()
True
>>> serializer.save()
<Task: Faire des courses>
>>> Task.objects.all()
<QuerySet [<Task: Réparer la fenêtre>, <Task: Faire des courses>]>
```

<!-- v -->

### La vue

La vue fonction `api_tasks`, qui répond sur `/api/v0/tasks`

```python
def api_tasks(request):
    """API Polls get and post"""
    if request.method == 'GET':
        task_list = Task.objects.all()
        serializer = TaskSerializer(task_list, many=True)
        response_data = {"results": serializer.data}
        return JsonResponse(response_data)
    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = TaskSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(data, status=201)
        return JsonResponse(serializer.errors, status=400)
```

<!-- v -->

## Autres exemples d'utilisation

### Mise à jour d'une instance

Récupérer la tâche dans un serializer

```python
>>> task1 = Task.objects.last()
>>> serializer = TaskSerializer(task1)
>>> serializer.data
{'title': 'Faire des courses>', 'deadline': '2021-01-08T10:35:29.549014Z'}
>>> data = serializer.data
```

Modifier la tâche 

```python
>>> data.title = 'Faire des courses>'
>>> update_serializer = TaskSerializer(task1, data)
>>> update_serializer.is_valid()
>>> update_serializer.save()
```

<!-- v -->

## TP<br> <small>Écrire la méthode d'update du serializer</small>

- Écrire une vue `api_task_detail` qui permet de récupérer un livre, de le mettre à jour et de le supprimer
- Écrire au moins un test pour chacune de ces opérations

<!-- v -->

Pour la mise à jour vous devrez ajouter une méthode update au serializer :

```python
class TaskSerializer(serializers.Serializer):
    # ...
    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.deadline = validated_data.get('deadline', instance.deadline)
        instance.save()
        return instance
```

<!-- v -->

## Serializers pour les models

- ModelSerializer
- HyperlinkModelSerializer

```python
class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ['id', 'title', 'deadline']
```

<!-- v -->

## TP<br> <small>Écrire la méthode d'update du serializer</small>

Utiliser le model serializer et vérifier que vos tests passent bien.

<!-- s -->

# Requêtes et réponses en Django REST Framework

<!-- v -->

Django REST Framework étend les classes HttpRequest et HttpResponse de Django :

* [Request](http://www.django-rest-framework.org/api-guide/requests/) 
ajoute notamment l'attribut `.data`, plus générique que `.POST`
* [Response](http://www.django-rest-framework.org/api-guide/responses/) 
ajoute de la négociation de contenu

Ces classes fonctionnent avec des *wrappers* de vues :

* le décorateur [@api_view](http://www.django-rest-framework.org/api-guide/views/#api_view)
* la classe [APIView](http://www.django-rest-framework.org/api-guide/views/#class-based-views)

<!-- v -->

## Utilisation dans nos vues

```python
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status


@api_view(['GET', 'POST'])
def api_tasks(request):
    """API Tasks get and post"""
    if request.method == 'GET':
        tasks_list = Task.objects.all()
        serializer = TaskSerializer(tasks_list, many=True)
        response_data = {"results": serializer.data}
        return Response(response_data)
    elif request.method == 'POST':
        serializer = TaskSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
```

Visiter [http://localhost:8000/api/v0/tasks/](http://localhost:8000/api/v0/tasks/) avec un navigateur et essayer la version HTML de l'API.

<!-- v -->

# TP <br><small>Request et Response dans la vue detail</small>

Modifier la vue `api_task_detail` pour qu'elle utilise `Request` et `Response`.

* s'assurer que nos tests passent toujours.  
* jouer avec la version HTML de l'API

<!-- s -->

# Les vues

<!-- v -->

## Vues de type classe

DRF Fournit la vue `APIView`, qui hérite de `View` de django.

- Elle utilise `Request` plutôt que `HTTPRequest`
- Elle renvoie `Response` plutôt que `HTTPResponse` (ou autre)

<!-- v -->

## Vue liste de tasks avec APIView
```python
from rest_framework.views import APIView

class TaskView(APIView):
    """API Polls get and post"""

    def get(self, request, format=None):
        tasks_list = Task.objects.all()
        serializer = TaskSerializer(tasks_list, many=True)
        response_data = {"results": serializer.data}
        return Response(response_data)

    def post(self, request, format=None):
        serializer = TaskSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
```

<!-- v -->

### TP

Modifier `api_task_detail` pour en faire une classe qui hérite de `APIView`

* s'assurer que nos tests passent toujours

<!-- v -->

## Les classes mixins

Les opérations CRUD sont souvent les même d'une vue à l'autre. Django REST Framework propose des mixins qui implémentent ces opérations.

Le site [cdrf.co](http://www.cdrf.co/) aide à comprendre les liens entre ces classes.

<!-- v -->

### Vue liste de tasks avec les mixins

```python
from rest_framework import mixins
from rest_framework import generics
from tasks.serializers import TaskSerializer


class TaskListView(mixins.ListModelMixin, mixins.CreateModelMixin,
               generics.GenericAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)
```

<!-- v -->

### TP

- Modifier `api_task_detail` pour en faire une classe qui utilisent les mixins

Voici les mixins dont vous aurez besoin :

<table>
<tr><th>Mixin</th><th>Méthode associée</th></tr>
<tr><td><a href="http://www.django-rest-framework.org/api-guide/generic-views/#retrievemodelmixin">RetrieveModelMixin</a></td><td>retrieve</td></tr>
<tr><td><a href="http://www.django-rest-framework.org/api-guide/generic-views/#updatemodelmixin">UpdateModelMixin</a></td><td>update</td></tr>
<tr><td><a href="http://www.django-rest-framework.org/api-guide/generic-views/#destroymodelmixin">DestroyModelMixin</a></td><td>destroy</td></tr>
</tbody>
</table>

<!-- v -->

- (Bonus) Factoriser le code commun entre `TaskListView` et `TaskDetail` dans une classe `TaskView`.

<!-- v -->

## Les vues génériques pré-mixées

Certaines combinaisons de mixins sont tellement courantes que DRF propose des vues qui incorporent déjà des mixins.

```python
class TaskListView(TaskView, generics.ListCreateAPIView):
    pass
```

<!-- v -->

### TP

Modifier `TaskDetail` pour qu'elle hérite de 
[`RetrieveUpdateDestroyAPIView`](http://www.django-rest-framework.org/api-guide/generic-views/#retrieveupdatedestroyapiview).

<!-- s -->

# TP<br><small>API endpoint pour les auteurs</small>

- Un serializer `AuthorSerializer`
- Une classe vue `AuthorListView`
- L'url correspondante sur `/api/v0/authors/`

<!-- v -->

## Root API

Configurer le point d'entrée racine de l'API sur `/api/v0/`

```python
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.decorators import api_view

@api_view(['GET'])
def api_root_view(request, format=None):
    return Response({
        'tasks': reverse('api_tasks', request=request, format=format),
        'authors': reverse('api_authors', request=request, format=format)
    })
```

<!-- s -->

# Les relations

<!-- v -->

## Relation `StringRelatedField`

[Représentation textuelle](http://www.django-rest-framework.org/api-guide/relations/#stringrelatedfield)

```python
class TaskSerializer(serializers.ModelSerializer):
    """Serializer Task basé sur Model Serializer"""

    author_set = serializers.StringRelatedField(many=True)

    class Meta:
        model = Task
        fields = ['id', 'title', 'deadline', 'done']
```

<!-- v -->

Ce qui donne

```json
"id": 1,
"title": "Réparer la fenêtre",
"deadline": "2021-01-08T10:35:29.549014Z",
"author_set": [
    "Franck Herbert",
]
```

<!-- v -->

## Relation `PrimaryKeyRelatedField`

[Clés primaires](http://www.django-rest-framework.org/api-guide/relations/#primarykeyrelatedfield)

```python
author_set = serializers.PrimaryKeyRelatedField(
    many=True,
    read_only=True
)
```

<!-- v -->

Ce qui donne

```json
…
"author_set": [
    1,
    2,
    3
]
```

<!-- v -->

## Relation `HyperlinkedRelatedField`

[Lien vers l'api détail](https://www.django-rest-framework.org/api-guide/relations/#hyperlinkedrelatedfield)

```python
author_set = serializers.HyperlinkedRelatedField(
    many=True,
    read_only=True,
    view_name='api_authors_detail'
)
```

<!-- v -->

Ce qui donne

```json
…
"author_set": [
    "http://127.0.0.1:8000/api/v0/authors/1",
    "http://127.0.0.1:8000/api/v0/authors/2",
    "http://127.0.0.1:8000/api/v0/authors/3"
]
```

<!-- v -->
## Relation `SlugRelatedField`

[slug](https://www.django-rest-framework.org/api-guide/relations/#slugrelatedfield)

```python
author_set = serializers.SlugRelatedField(
    many=True,
    read_only=True,
    slug_field='lastname'
)
```

*En écriture, le champs doit être défini avec une contrainte d'unicité*

<!-- v -->

Ce qui donne

```json
…
"author_set": [
    "Herbert",
]
```

<!-- v -->

## Sérializer personnalisé

On peut ajouter un serializer en tant que champ d'un autre serializer

```python
authors = AuthorSerializer(
    many=True,
    read_only=True,
)
```

<!-- v -->

```json
"authors": [
    {
        "lastname": "Le Guin",
    },
    {
        "lastname": "Herbert",
    },
    {
        "lastname": "Pratchett",
    }
]
```

<!-- v -->

# TP

Utiliser une relation par un lien

<!-- v -->

## POST

Créer un nouveau livre 

```shell
curl --location --request POST 'http://127.0.0.1:8000/api/v0/authors/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "title": "Tehanu"
}'
```

Réponse

```json
{
    "id": 12,
    "title": "Tehanu",
    "deadline": null,
    "authors": []
}
```

<!-- v -->

## PUT

Mettre à jour la tâche 12

```shell
curl --location --request PUT 'http://127.0.0.1:8000/api/v0/tasks/11' \
--header 'Content-Type: application/json' \
--data-raw '{
    "title": "Préparer les pancartes",
}'
```

<!-- v -->

## Avec un auteur

Essayez les données suivantes

```shell
curl --location --request PUT 'http://127.0.0.1:8000/api/v0/tasks/11' \
--header 'Content-Type: application/json' \
--data-raw '{
    "title": "Le vent d'ailleurs",
    "authors": [
        {
            "firstname": "Ursula",
            "lastname": "Le Guin",
        }
    ]
}'
```

<!-- v -->

Il faut implémenter une méthode de création du livre pour intégrer les auteurs.

TP : inspirez-vous de [cet exemple](https://www.django-rest-framework.org/api-guide/relations/#writable-nested-serializers) pour renseigner l'auteur d'un la requête de création du livre.

<!-- s -->

# Les ViewSets

<!-- v -->

* Type de vue qui ne gère pas directement les méthodes HTTP comme GET ou POST 
mais fournit des actions comme `list` ou `create`
* On lie ces actions à des méthodes HTTP au moment de finaliser la vue avec `as_view`
* Ils permettent donc de faire facilement du CRUD sur une (ou plusieurs) ressource

<!-- v -->
**On regroupe nos vues dans un viewset**

```python
from rest_framework import viewsets

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by("username")
    serializer_class = UserSerializer
```

**Mise à jour de la conf des URLs**


```python
user_list = views.UserViewSet.as_view({'get': 'list', 'post': 'create'})
user_detail = views.UserViewSet.as_view({'get': 'retrieve', 'put': 'update',
                                         'delete': 'destroy'})
urlpatterns = [
    url(r'^api/v1/users/$', user_list),
    url(r'^api/v1/users/(?P<pk>\d+)/$', user_detail),
]
```

<!-- v -->

**Nouvelle classe de permission**

```python
class UserPermission(BasePermission):
    def has_permission(self, request, view):
        if view.action not in ("list", "create"):
            return True
        if request.method in SAFE_METHODS:
            return request.user.is_staff
        else:
            return request.user.is_superuser

    def has_object_permission(self, request, view, obj):
        if request.user.is_superuser:
            return True
        if request.user.is_staff and request.method in SAFE_METHODS:
            return True
        return obj == request.user
```

<!-- s -->

# Les routeurs

<!-- v -->

Les routeurs génèrent une liste d'URLs à partir des viewsets enregistrés.

**Nouvelle configuration d'URLs**

```python
from django.conf.urls import url, include
from rest_framework import routers
from staff import views

router = routers.DefaultRouter()
router.register("users", views.UserViewSet)


urlpatterns = [
    url(r'^api/v0/', include(router.urls)),
]
```

<!-- s -->

# Et aussi...

* [Les permissions](https://www.django-rest-framework.org/api-guide/permissions/)
* [Le throttling](http://www.django-rest-framework.org/api-guide/throttling/) pour limiter le nombre d'accès
* [Les schemas](http://www.django-rest-framework.org/api-guide/schemas/) pour fournir une description de son API
* customiser [la pagination](http://www.django-rest-framework.org/api-guide/pagination/)
* l'authentification [par tokens](http://www.django-rest-framework.org/api-guide/authentication/#tokenauthentication)
* et bien [plus encore](http://www.django-rest-framework.org/#api-guide)...
