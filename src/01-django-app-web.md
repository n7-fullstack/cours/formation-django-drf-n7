---
title: Django comme une application web complète
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---

# Django comme application web

<!-- v -->

## Sommaire

* Vues, Templates et Urls
* Formulaires

<!-- s -->

# Les vues

<!-- v -->

## Vues

Une vue contient la logique nécessaire pour renvoyer une réponse, elle pourrait
collecter des données, soumettre un formulaire, afficher du HTML, du JSON ou du
XML, générer un fichier zip ou même envoyer un e-mail dans son traitement.

<!-- v -->

## Vue qui liste les questions

```python
# questions/views.py
from polls.models import Question

def questions_view(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    output = ', '.join([q.question_text for q in latest_question_list])
    return HttpResponse(output)
```

Ce style de vue est dit *function-based* (par opposition à *class-based*).

<!-- v -->

Transformons la vue pour renvoyer un template avec un context

```python
# questions/views.py
from django.shortcuts import render
from polls.models import Question

def questions_view(request):
    # Récupération des questions
    questions = Question.objects.all()

    # Définition d'un contexte à fournir au template
    context = {'latest_question_list': questions}

    # Rendu du gabarit
    return render(request, 'index.html', context)
```

<!-- v -->

### Création du template

Un template est spécialement conçu pour renvoyer du HTML en fonction d'un contexte
(dans lequel sont contenus les variables).

```html
<!-- polls/templates/index.html -->
{% if latest_question_list %}
    <ul>
    {% for question in latest_question_list %}
        <li><a href="/polls/{{ question.id }}/">{{ question.question_text }}</a></li>
    {% endfor %}
    </ul>
{% else %}
    <p>No polls are available.</p>
{% endif %}
```

<!-- v -->

## Vue détail d'une question

```python
# questions/views.py
from polls.models import Question

def detail(request, question_id):
    return HttpResponse("Question %s." % question_id)
```

<!-- v -->

### Déclaration de l'URL

```python
# polls/urls.py
from django.urls import path
from polls import views

app_name = 'polls'
urlpatterns = [
    #...
    path('<int:question_id>/', views.detail, name='detail'),
]
```

<!-- v -->

### Template du détail

```html
<!-- polls/templates/question_detail.html -->
{# polls/templates/polls/question_detail.html #}

<h1>Question {{ question.id }} :<br>
  {{ question.question_text }}</h1>

{% if question.choice_set.all %}
    <form action="/polls/vote/" method="POST">
        {% for choice in choice_set.all %}
        <div>
          <input type="radio" id="{{ choice.choice_text }}" name="vote" value="{{ choice.choice_text }}">
          <label for="{{ choice.choice_text }}">{{ choice.choice_text }}</label>
        </div>
        {% endfor %}
    </form>
{% else %}
    <p>Pas de choix !</p>
{% endif %}
```

Documentation: <https://docs.djangoproject.com/fr/stable/topics/templates/>

<!-- s -->

# Le moteur de template

Vues et templates : extends, block, tags et filtres

<!-- v -->

## Qu'est-ce qu'un template Django ?

C'est un simple fichier texte qui peut générer n'importe quel format de texte (HTML, XML, CSV, ...).

Un template a accès à des **variables** qui lui auront été passées via un **contexte** par la vue.

Par défaut, Django fournit sa propre syntaxe de template mais il est possible de la remplacer par un autre moteur comme Jinja2.

<!-- v -->

### Où écrire ses templates ?

Pour retrouver les templates d'un projet, Django se base sur le réglage ``TEMPLATES``. Le plus souvent on stocke les templates :

* dans chaque application, en suivant l'arborescence ``<app>/templates/<app>``.
  Ils seront retrouvés grâce au loader activé par défaut quand la clé ``APP_DIRS`` vaut `True`.
* dans un répertoire ``templates/`` à la racine du projet qu'il faudra déclarer dans la clé ``DIRS``.

<!-- v -->

Dans ce mécanisme de découverte, l'ordre importe : cela permet de surcharger les templates d'autres applications.

<!-- v -->

## Syntaxe de Django template

### Affichage d'une variable

```django
{{ ma_variable }}
```

<!-- v -->

### Les filtres

Il est possible de modifier l'affichage d'une variable en appliquant des **filtres**. Un filtre peut prendre (ou non) un argument. Les filtres peuvent être appliqués en cascade. Quelques exemples :

```django
{{ name|lower }}
{{ text|linebreaksbr }}
{{ current_time|time:"H:i" }}
{{ weight|floatformat:2|default_if_none:0 }}
```

Django fournit nativement une liste de filtres assez intéressante et il est possible d'écrire des filtres personnalisés facilement.

<!-- v -->

### Les tags

Les **tags** sont plus complexes que les variables, ils peuvent créer du texte ou de la logique (boucle, condition, ...) dans la tempate.

* **Une condition *if***

```django
{% if condition %} .. {% else %} .. {% endif %}
```

* **Une boucle *for***

```django
{% for item in list %} .. {% endfor %}
```

<!-- v -->

* **Un lien avec *url***

```django
<a href="{% url 'books:book_detail' book.pk %}">Django book</a>
```

Django fournit aussi plusieurs tags nativement et il est possible d'écrire ses propres tags.

<!-- v -->

## L'héritage de template

L'intérêt de l'héritage de template est par exemple de pouvoir créer un squelette HTML contenant tous les éléments communs du site et définir des blocs que chaque template pourra surcharger.

Dans un template *parent*, la balise ``{% block %}`` permet de définir les blocs surchargeables.

Dans un template *enfant*, la balise ``{% extends %}`` permet de préciser de quel template celui-ci doit hériter.

<!-- v -->

### Exemple de template *parent*

```django
{# templates/base.html #}
<html>
  <head>
    <title>
      {% block title %}
        ...
      {% endblock %}
    </title>
    <link href="styles.css" rel="stylesheet" />
  </head>
  <body>
    <header>Entête commune à tout le site</header>
    <section>
      {% block content %}
        ...
      {% endblock %}
    </section>polls/1/results
    <footer>Pied de page commun à tout le site</footer>
  </body>
</html>
```

<!-- v -->

### Exemple de template *enfant*

```django
{# books/templates/books/book_list.html #}

{% extends "base.html" %}

{% block title %}
  Liste des livres
{% endblock %}

{% block content %}
  {% if books %}
    <ul>
      {% for book in books %}
        <li>{{ book }}</li>
      {% endfor %}
    </ul>
  {% else %}
    <p>Aucun livre !</p>
  {% endif %}
{% endblock %}
```

<!-- v -->

## L'inclusion de template

L'intérêt de l'inclusion de template est de pouvoir factoriser du code de template :

* pour éviter d'avoir des fichiers de templates trop long
* pour le réutiliser facilement tout en évitant la duplication de code

<!-- v -->

Cela peut être utile dans différents cas :

* pour certains éléments communs de la page (menu, entête, pied de page, ...)
* pour certaines macros réutilisables (structure d'onglets, affichage en liste d'éléments, structure HTML d'une pop-in, ...)

<!-- v -->

### Exemple de template *appelant*

```django
{# templates/base.html #}
<html>
  <head>
    <title>
      {% block title %}
        ...
      {% endblock %}
    </title>
    <link href="styles.css" rel="stylesheet" />
  </head>
  <body>
    {% include 'header.html' %}
    <section>
      {% block content %}
        ...
      {% endblock %}
    </section>
    {% include 'footer.html' %}
  </body>
</html>
```

<!-- s -->

# TP : templates

- créer une vue pour la liste de tâches
- créer une vue pour une tâche détail

<!-- s -->

# Et ensuite

* Django permet de réaliser une appli web complète
* Système de vues, templates, formulaires…
* Dans les prochaines séance : créer une API utilisable par une autre appli web

Note:

Il est possible de n'utliser que Django pour réaliser une application web de bout en bout. Des vues listant les objets ou bien des formulaires de saisie peuvent être créés.

Dans notre cours, nous allons nous concentrer sur la réalisation d'une API utilisable par une autre

TODO: Faire une vue liste et une vue détail
