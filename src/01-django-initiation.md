---
title: Django initiation
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---

# Initiation à Django

<!-- v -->

## Sommaire

* Introduction
* Installation
* Structure
* Requêtes et réponses
* Modèles
* ORM
* Le shell Django


<!-- s -->

# Introduction

<!-- v -->

## Qu'est-ce-que Django ?

* Framework en Python pour le Web
* Django permet de construire des applications web rapidement et avec peu de code
* Malgré son haut niveau d'abstraction, il est toujours possible de descendre dans les couches

<!-- v -->

## Historique

* Créé en 2003 par le journal local de Lawrence (Kansas, USA), basé sur le langage Python créé en 1990
* Rendu Open Source (BSD) en 2005
* Django 1.x : compatible python 2 et 3
* Django 2+ : compatible python 3 uniquement
* Aujourd'hui utilisé par de très nombreuses entreprises/sites :
  Mozilla, Instagram, Pinterest, Disqus, National Geographic, ...

<!-- v -->

## Philosophie

> La Plateforme de développement Web pour les perfectionnistes sous pression.

> <cite> — <https://docs.djangoproject.com/fr/></cite>

<!-- v -->

### KISS (*Keep It Simple, Stupid*)

> Dans le design, la simplicité devrait être la règle et la complexité devrait être évitée si elle n'est pas nécessaire.

<!-- v -->

### DRY (*Don't Repeat Yourself*)

> Dans un système, toute connaissance doit avoir une représentation unique, non-ambiguë, faisant autorité – Source : [Wikipedia](https://fr.wikipedia.org/wiki/Ne_vous_r%C3%A9p%C3%A9tez_pas)

<!-- v -->

### Définition

> [Framework](https://fr.wikipedia.org/wiki/Framework) : ensemble cohérent de composants logiciels structurels qui sert à créer les fondations ainsi que les grandes lignes de tout ou partie d'un [logiciel](https://fr.wikipedia.org/wiki/Logiciel "Logiciel"), c'est-à-dire une [architecture](https://fr.wikipedia.org/wiki/Architecture_informatique "Architecture informatique").

En résumé : pas besoin de recoder la roue.

<!-- v -->

### Composants de Django

Django met à disposition :

- Un ensemble de classes pour gérer types de contenus et champs, formulaires, requêtes, vues et affichages
- La mécanique de gestions de la base de données
- Un système de droits et de permissions
- Une interface d'administration des contenus métiers
- Des composants pour les tests automatisés
- …

<!-- v -->

### Les bonnes raisons d'utiliser Django

* Facile à installer
* Très complet
* Excellente documentation (en français)
* Modèles en Python et ORM faciles à utiliser
* Interface d'administration auto-générée
* Gestion de formulaires
* Serveur de développement inclus
* Extensible, nombreux modules existants
* Communauté autour du projet très active

<!-- v -->

## TP pour le tutoriel

Application de sondage 

→ TP : une liste de tâches

Les exemples sont tirés du tutoriel Django, à adapter pour notre TP.

<!-- s -->

# Environnement

<!-- v -->

## Avec quoi un projet peut fonctionner ?

* Python : 3.7+
* Django 3.2 (LTS), 4.0+
* Base de données : SQLite, PostgreSQL, MySQL

<!-- v -->

### Conventions de codage

La [documentation](https://docs.djangoproject.com/fr/3.0/internals/contributing/writing-code/coding-style/) précise certaines conventions de codage spécifiques à Django. 

Pour le reste : la [PEP 8](https://www.python.org/dev/peps/pep-0008/) (conventions de codage en python)

<!-- v -->

### Côté python

Python parcourt sys.path pour chercher les modules à importer

* Par défaut ce path contient les répertoires systèmes tels que ``/usr/lib/python``,
``/usr/local/lib/python``, ``~/.local/lib/python`` ainsi que le répertoire courant
* Comme tout module python, Django doit être accessible dans le path pour être utilisé
* __Virtualenv__ permet de créer un environnement python en isolation du système, méthode préférable pour développer avec python

<!-- v -->

### Introduction au virtualenv

__Virtualenv__ permet de créer un environnement python en isolation du système,
c'est la méthode préférable pour développer avec python

```console
$ python3 -m venv venv  # crée l'environnement virtuel dans le dossier `venv`
$ ./venv/bin/python3  # lance le python de l'environnement virtuel
$ source venv/bin/activate  # ajoute ./venv/bin en tête du PATH
(venv) $ python  # lance le python de l'environnement virtuel
(venv) $ deactivate  # rétablit le path
$ python3  # lance le python du système
```

Quelques alternatives/extensions : _anaconda_, _pyenv_, _pipenv_.

<!-- v -->

## Bases de données

* SQlite : un simple fichier
* PostgreSQL : la plus adaptée pour la production
* MySQL / MariaDB : possible également

<!-- s -->

# Structure d'un projet Django

<!-- v -->

## _Model_, _Template_, _View_ ?

S'inspire du principe MVC (*Model, View, Controller*)  
ou plutôt MTV (*Model, Template, View*) :

* __Model__ : définition des objets et leurs attributs / Django ORM pour l'accès à la base de données **→  données**
* __Template__ : affichage des données **→ comment les présenter**
* __View__ : fonction ou classes retournant une réponse HTTP + contexte en fonction des accès **→ quelles données présenter**

<small>cf [l'explication selon les créateurs](https://docs.djangoproject.com/fr/3.1/faq/general/#django-appears-to-be-a-mvc-framework-but-you-call-the-controller-the-view-and-the-view-the-template-how-come-you-don-t-use-the-standard-names)</small>

<!-- v -->

## Gestion des URLs
L'_URL dispatcher_ : fait correspondre **URLs** et **vues**

<!-- v -->

## Projet vs. Application

Il est important de différencier la notion de **projet** et d'**application**.

### Une application

Application Web qui fait quelque chose – par exemple un système de blog ou une application de sondage

### Un projet

Ensemble de réglages et d’applications pour un site Web particulier.

<!-- v -->

### Projets et applications

Un projet peut contenir plusieurs applications. Une application peut apparaître dans plusieurs projets.

Un paquet de projet peut aussi être considéré comme une application (pour qu’il contienne des modèles, etc.).

> Un doute ? Consulter [la documentation sur le sujet](https://docs.djangoproject.com/fr/stable/ref/applications/#projects-and-applications)

<!-- v -->

### Un projet est une combinaison d'applications

* Le projet peut être découpé en différentes apps
* Une même app peut être réutilisée dans plusieurs projets
* Ce sont des modules python
* Django fournit par défaut des apps, par exemple pour gérer l'authentification
* Nombreuses apps mises à disposition par la communauté (installation via `pip`)

<!-- v -->

### Projet django typique

* des apps de Django
* d'autres provenant de la communauté
* une ou des apps spécifiques au projet

<!-- v -->

### Déclarer une app dans le projet

* La commande `manage.py startapp` crée automatiquement un patron d'app dans un nouveau répertoire
* Les apps sont à déclarer dans les settings ( `INSTALLED_APPS = [...]` )
* C'est grace à ce settings que django sait différencier les applications des modules python disponibles dans le path.

<!-- v -->

## Structure d'une application : chaque chose à sa place

Django "impose" une organisation du code (noms et emplacements des fichiers)

C'est une contrainte mais c'est aussi très pratique pour retrouver son code,
  quand c'est le sien ou quand c'est celui des autres : ce qui revient au même au bout d'un certain temps...

<!-- v -->

* Par exemple les vues vont dans le  `views.py` dans le répertoire de l'app (ou dans le package `views` de l'app)
* Et les URLs vont dans le module `urls` (fichier `urls.py`)
* On peut inclure la liste des URLs d'une app dans les URLs du projet

```python
from django.urls import include, path
urlpatterns = [
    # soit on définit des couples url-vue
    path('', une_vue),
    # ... ou on inclut les urls depuis une autre app
    path('', include('books.urls')),
]
```

<!-- s -->

# TP : installer Django, projet et app

<!-- v -->

## Installer et activer un _Virtualenv_

```!console
$ sudo apt install python3-venv # si ce n'est pas déjà fait
$ python3 -m venv venv
```

## Installation de Django

```!console
$ source venv/bin/activate
(venv) $ pip install django
```

<!-- v -->

## Créer le projet

```!console
(venv) $ django-admin startproject monprojet
```

N'appelez pas votre projet "django" ou "test", ce sont des mots utilisés par Django.

<!-- v -->

## Le serveur de développement de Django

Django inclut un serveur HTTP de développement (à ne surtout pas utiliser en production pour des raisons de performances et de sécurité)

* Ce serveur se relance (presque toujours) automatiquement lorsqu'il détecte un changement de fichier
* Par défaut il écoute sur l'interface localhost (`127.0.0.1`) sur le port `8000`
  
<!-- v -->

* Le script `manage.py` fait la même chose que `django-admin` mais après avoir lu la configuration du projet (dans ``settings.py``)
* Attention !  Il peut s'arrêter inopénément si vous écrivez des bêtises (erreur de syntaxe, classe mal déclarée, etc.), dans ces cas, il faudra le redémarrer

<!-- v -->

## Lancer le serveur

```!console
(venv) $ cd monprojet
(venv) $ ./manage.py runserver
```

<!-- v -->

### It works !

![](./img/00-run-server.png)

<!-- v -->

## Créer l'application

```!console
(venv) $ ./manage.py startapp polls
```

<!-- v -->

## Structure du projet

```!console
monprojet
├── db.sqlite3
├── manage.py
├── monprojet
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
└── polls
    ├── admin.py
    ├── apps.py
    ├── migrations
    ├── models.py
    ├── tests.py
    └── views.py
```

<!-- v -->

### Fichiers du projet

* `monprojet` : conteneur du projet et des applications (le nom est sans importance)
* `manage.py` : utilitaire en ligne de commande permettant différentes actions sur le projet
* `monprojet` : paquet Python effectif du projet

<!-- v -->

* `monprojet/settings.py` : réglages et configuration du projet
* `monprojet/urls.py` : déclaration des URLs du projet
* `monprojet/wsgi.py` : point d'entrée pour déployer le projet avec WSGI
* `polls` : l'une des applications du projet (vous pouvez en créer plusieurs)

<!-- v -->

### Fichiers de l'application

* ``models.py`` : déclaration des modèles de l'application
* ``views.py`` : écriture des vues de l'application
* ``admin.py`` : comportement de l'application dans l'interface d'administration
* ``tests.py`` : Il. Faut. Tester.
* ``migrations``: modifications successives du schéma de la base de données

<!-- v -->

## Activer l'application

```python
# settings.py
INSTALLED_APPS = [
    'django.contrib.admin',
    ...
    'polls.apps.PollsConfig'
]
```

<!-- s -->

# Ma première vue

<!-- v -->

## Créer une vue page d'accueil pour l'application Polls

`index` reçoit la requête en paramètre, et renvoie une réponse.

```python
# polls/views.py
from django.http import HttpResponse

def index(request):
    return HttpResponse("Bienvenue sur l'application Sondages !")
```

<!-- v -->

## Ajouter une URL

Déclarer un chemin et le relier à la vue

```python
# polls/urls.py (il n'existe pas, vous devrez le créer)
from django.urls import path
from polls import views

urlpatterns = [
    path('', views.index),
    # …
]
```

<!-- s -->

# Les vues

<!-- v -->

## Function-based views

Une vue *basée sur une fonction* est une fonction Python qui prend
en entrée une **requête HTTP** et retourne une **réponse HTTP**.

Cette réponse peut être une page HTML, un document XML, une redirection, une erreur
404, un fichier PDF ou zip généré à la volée…

Ces vues sont écrites dans le fichier `views.py` ou dans un package `views/` de l'application.

<!-- v -->

### Un exemple tiré de la documention Django

```python
# some_app/views.py
from django.http import HttpResponse
import datetime

def current_datetime(request):
    now = datetime.datetime.now()
    html = "<html><body>It is now %s.</body></html>" % now
    return HttpResponse(html)
```

<!-- v -->

## Class-based views

Une vue *basée sur une classe* est une classe qui prend en entrée une **requête HTTP**
et retourne une **réponse HTTP** à travers sa méthode  `dispatch()` qui appelle la
méthode `get` ou `post()` selon le verbe HTTP.

<!-- v -->

### Un exemple tiré de la documentation Django

```python
# some_app/views.py
from django.http import HttpResponse
from django.views.generic import View

class CurrentDatetimeView(View):
    def get(self, request, *args, **kwargs):
        now = datetime.datetime.now()
        html = "<html><body>It is now %s.</body></html>" % now
        return HttpResponse(html)
```
  			
<!-- v -->

L'URL correspondante avec une class-based view (CBV) aura cette forme :

```python
# some_app/urls.py
from django.urls import path
from some_app.views import CurrentDatetimeView
urlpatterns = [
    path('date-courante', CurrentDatetimeView.as_view()),
]
```

<!-- s -->

# L'URL dispatcher

<!-- v -->

## Processus de traitement des requêtes

1. Django identifie le module *URLconf* racine à utiliser (cf `ROOT_URLCONF` dans les *settings*), souvent `projet/urls.py`
2. Django charge ce module et cherche la variable ``urlpatterns``
3. Django parcourt chaque motif d’URL dans l’ordre et s’arrête dès la première
correspondance avec l’URL demandée. Il tient compte des _include_ d'autres `urls.py` de chaque application.
   
<!-- v -->

4. Une fois qu’une des regex correspond, Django appelle la vue correspondante
avec en paramètre la requête HTTP (objet Python ``HttpRequest``) puis toutes les valeurs capturées dans la regex.
5. Si aucune regex ne correspond, ou si une exception est levée durant ce
processus, Django appelle une vue d’erreur appropriée.

<!-- v -->

## Écriture d'une configuration d'URL

Le paramètre `ROOT_URLCONF` est un module (sourvent ``projet/urls.py``) contenant une variable ``urlpatterns`` :

```python
# library/urls.py
from django.urls import path
urlpatterns = [
    path('', home, name='home'),
    path('contact', ContactView.as_view(), name='contact'),
    # autant de motifs d'URL que nécessaire...
]
```

À chaque motif peut être associé un nom système qui pourra servir lors de l'inversion d'une URL.

<!-- v -->

## Inclusion d'*URLconf*

Souvent, l'*URLconf* racine incluera les modules URLconf de chaque 
application :

```python
# library/urls.py
from django.urls import include, path
urlpatterns = [
    path('', home, name='home'),
    path('contact', ContactView.as_view(), name='contact'),
    path('questions/', include('questions.urls')),
]
```

À chaque application peut être associé un namespace qui
pourra servir lors de l'inversion d'une URL.

<!-- v -->

## Syntaxe de déclaration d'une URL

### URL sans paramètre

```python
path('myview', my_view, name='my_view')
```

La vue aura en argument seulement l'objet ``HttpRequest``.

```python
def my_view(request):
    pass
```

<!-- v -->

### URL avec paramètres


La vue aura en argument l'objet ``HttpRequest``, puis les valeurs trouvées dans l'expression régulière.

```python
path('archives/<int:year>/<int:month>/', ArchiveView.as_view(), name='archive'),
```

Exemple avec l'URL `http://127.0.0.1:8000/archives/2014/12/`

```python
class ArchiveView(View):
    def get(self, request, year, month):
        print(year)  # 2014
        print(month)  # 12
```

<!-- v -->

## Résolution inversée d'une URL

La résolution consiste à partir d'une URL et trouver le motif correspondant ainsi que ses paramètres.

`/archives/2018/12/` **->** `'archive' year=2018 month=12`

La résolution inversée part d'un nom système de motif avec des paramètres pour arriver à une URL.

`'archive' year=2019 month=3` **->** `/archives/2019/03/` 

Ceci permet de modifier les motifs sans avoir à retoucher toutes les fois où
l'URL est appelée (par exemple dans un `<a href>`).

<!-- s -->

# Mon premier modèle

<!-- v -->

## Générer la structure de la base de données

Pour le moment, nous gardons le connecteur de base de données par défaut : SQlite3.

```!console
$ ./manage.py migrate
```

_Remarque : par défaut Django utilise SQLite, en production PostgreSQL est plus indiqué._

<!-- v -->

## Le modèle Question et ses champs

* Question text → CharField
* Date de publication → DateField

[Documentation sur les champs](https://docs.djangoproject.com/fr/3.0/ref/models/fields/)

Notes:

_todo : schéma de données ?_

<!-- v -->

## Déclarer le modèle

```python
# polls/models.py
from django.db import models

class Question(models.Model):
    """Model for Question"""
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
```

<!-- v -->

## Générer et appliquer la migration

```console
(venv) $ ./manage.py makemigrations
(venv) $ ./manage.py migrate
```

<!-- v -->

## Types de champs

* Texte `CharField`, `EmailField`…
* Nombres : `IntegerField`, `FloatField`, `DecimalField`…
* Booléens :  `BooleanField` et `NullBooleandField`
* Dates : `DateField`, `TimeField`, `DurationField`…
* Fichiers : `FileField`, `ImageField`…

<!-- v -->

### Les champs texte

* `CharField` (une ligne avec longueur max)
* `TextField` (multiligne)
* `EmailField` (vérifie la syntaxe de l'adresse)

<!-- v -->

### Les champs pour les nombres

* `IntegerField` et `PositiveIntegerField`
* `FloatField`
* `DecimalField` (précision fixe, non soumis aux arrondis)
* `AutoField` (`IntegerField` incrémenté automatiquement)

<!-- v -->

### Booléens, dates, fichiers

* Les champs booléens :  `BooleanField` et `NullBooleandField`
* Les champs pour les dates :
    * `DateField`, `TimeField` et `DateTimeField`
    * `DurationField`
* Les champs pour la gestion des fichiers :
    * `FileField` et `ImageField`
    * `FilePathField`

<!-- v -->

## Options de champs

* ``verbose_name``: label du champ
* ``null`` : valeur NULL autorisée ou non en base de données
* ``blank`` : valeur vide autorisée lors de la validation du champ dans un formulaire
* ``default`` : valeur par défaut pour une nouvelle instance

<!-- v -->

* ``editable`` : le champ doit-il apparaître automatiquement dans les formulaires
* `choices` permet d'expliciter la liste de valeurs possibles
* `primary_key` est la clé primaire (remplace *id*)
* `unique` ajoute une contrainte d'unicité
* `validators` permet d'ajouter des contraintes de validation au niveau du modèle

_cf_ [documentation sur les champs de modèle](https://docs.djangoproject.com/fr/stable/ref/models/fields/#field-options)

<!-- s -->

# Les migrations

<!-- v -->

Django permet de faire évoluer les modèles sans effacer les données,
en génèrant des « diffs » appelés migrations, qu'il applique ensuite à la base de données

<!-- v -->

## Génération des migrations

`./manage.py makemigrations`

1. Compare la dernière migration aux modèles déclarés
2. Génère un nouveau fichier de migration

<!-- v -->

## Application des migrations

`./manage.py migrate`

1. Convertit en SQL la ou les migrations qui n'ont pas encore été appliquées
2. Exécute le code SQL sur la base de donnée

<!-- v -->

## Remarques

La liste des migrations déjà faites est stockée dans la table
`django_migrations` en base de donnée.

Les fichiers de migrations sont numérotés et rangés dans `migrations/`.

<!-- v -->

Le SQL généré dépend de la base de donnée utilisée. Pour visualiser les modifications apportées à la base de donnée

```!console
$ ./manage.py sqlmigrate polls 0001
```

<!-- v -->

Il est possible de modifier ces fichiers python pour faire des migrations de données.
*Exemple : conversion d'un champ texte adresse en plusieurs champs (n°, rue, ville, cp)*

_cf_ [documentation sur les migrations](https://docs.djangoproject.com/fr/stable/topics/migrations/)

<!-- s -->

# L'administration Django

<!-- v -->

## Créer un super utilisateur

```!console
$ ./manage.py createsuperuser
```

Puis se connecter sur <http://127.0.0.1:8000/admin/>

## Déclarer le modèle dans l'admin

```python
### polls/admin.py
from django.contrib import admin
from polls.models import Question

admin.site.register(Question)
```
<!-- v -->

## Modèle dans l'interface admin

![](./img/03-admin.png)

<!-- v -->

## "back-office automatique" de Django

Liste les instances et par introspection des modèles, crée les formulaires de
création/modification correspondants.

<!-- v -->

Personnalisable, permet de modifier :

* les filtres et l'ordre des listes
* l'affichage des listes
* les formulaires et l'ordre des champs
* ajouter des actions en masse sur les listes

_cf_ [documentation sur l'interface d'administration](https://docs.djangoproject.com/fr/stable/ref/contrib/admin/)

<!-- v -->

## Personnaliser la représentation de l'objet

```python
class Question(models.Model):
    # ...
    def __str__(self):
        return self.question_text
```

![](./img/admin_str.png)

<!-- s -->

# Le shell Django

<!-- v -->

## Manipuler les objets


```!console
$ ./manage.py shell
```

### Lister les objets d'un modèle

```python
>>> from polls.models import Question
>>> Question.objects.all()
<QuerySet []>
```

<!-- v -->

### Créer un objet

```python
>>> from django.utils import timezone
>>> q = Question(question_text="Comment ça va ?", pub_date=timezone.now())
>>> q
<Question: Question object (None)>
```

Sauvegarder l'objet dans la base de données

```python
>>> q.save()
>>> q.id
1
>>> q
<Question: Question object (1)>
```
<!-- v -->

### Manipuler l'objet

```python
>>> q.question_text = "Ça roule ?"
>>> q.save()
```

<!-- v -->

### Personnaliser la représentation de l'objet

```python
class Question(models.Model):
    # ...
    def __str__(self):
        return self.question_text
```

```python
>>> from polls.models import Question
>>> Question.objects.all()
<QuerySet [<Question: Ça roule ?>]>
```

Note:

Quitter le shell et s'y reconnecter pour que ça soit pris en compte.

<!-- s -->

# Relations entre les modèles

<!-- v -->

## Les champs de relations

Champs spécifiques pour représenter les relations entre modèles.

* `models.ForeignKey` : relation __1-N__
* `models.ManyToManyField` : relation __N-N__
* `models.OneToOneField` : relation __1-1__

La troisième est beaucoup moins courante

<!-- v -->

## Relation __1-N__ : `models.ForeignKey`

* Premier argument le modèle auquel il fait référence
* `related_name`: nomme la relation inverse
* En base de donnée : contrainte de type clé étrangère

Note:

Le champ `ForeignKey` doit être déclaré avec comme premier argument le modèle
auquel il est lié par cette relation 1-N. L'argument optionnel `related_name`
permet de nommer la relation inverse à partir de ce modèle lié.

La représentation de ce champ en base de données est une contrainte de type clé étrangère.

<!-- v -->

## Relation __N-N__ : `models.ManyToMany`

* Premier argument le modèle auquel il fait référence
* `related_name`: nomme la relation inverse
* En base de donnée : une table de relation intermédiaire contenant les paires de clés primaires est créée

<!-- v -->

## Exemple : 1 question ←→ N choix.

Un deuxième modèle `Choice`

```python
# polls/models.py
class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
```

<!-- v -->

### Dans django shell

On crée une nouvelle question

```python
from django.utils import timezone
question = Question(question_text="Votre couleur préférée ?")
question.save()
```

On crée des choix pour cette question

```python
question.choice_set.create(choice_text='Bleu', votes=0)
question.choice_set.create(choice_text='Orange', votes=0)
choice = question.choice_set.create(choice_text='Violet', votes=0)
```

<!-- v -->

Accéder aux objets

```python
choice.question
question.choice_set.all()
question.choice_set.count()
```

<!-- v -->

### TP

- Créer le modèle liste de tâches `TaskList`
  - champs : nom
  - ajouter au modèle `Task` un champs ForeignKey vers `TaskList`
- Effectuer les migrations

<!-- s -->

# L'ORM

<!-- v -->

## Les moteurs de base de données

* 4 moteurs disponibles dans l'ORM de Django
    * PostgreSQL `django.db.backends.postgresql`
    * MySQL `django.db.backends.mysql`
    * Oracle `django.db.backends.oracle`
    * SQLite `django.db.backends.sqlite3`

<!-- v -->

* SQLite non recommandé en production
* Django est d'abord pensé pour PostgreSQL (champs `DateRangeField`, `JSONField`, extension PostGIS, recherche plein texte, etc.)
* Moteur spécifié dans `settings.py` (variable `DATABASES`), ainsi que la
  configuration du nom de la base, du serveur, et de l'authentification

_cf_ [documentation sur les bases de données](https://docs.djangoproject.com/fr/stable/ref/databases/)

<!-- v -->

## ORM (Object-relational mapping)

* Fait correspondre une classe Python à une table SQL
* Fait correspondre un objet python, instance de cette classe, à un
  enregistrement de cette table SQL
* Il y a donc juste des classes et objets python à manipuler, aucun SQL à
  écrire, que ce soit :
    * Pour créer et modifier les tables
    * Pour créer et modifier les données
    * Pour interroger la base

<!-- v -->

* Facilite la gestion des relations entre modèles (jointures)
* Mais nécessite souvent des optimisations

<!-- v -->

## Manipulation d'une instance de modèle

Pour créer un objet en base, il suffit de l'instancier en passant en argument les noms
des attributs du modèle. L'instance dispose ensuite d'une méthode ``save`` qui permet
de l'enregistrer en base de données.

```python
>>> b = question(question_text='Two scoops of django',
             pub_date=date(2013, 8, 31))
>>> b.save()
```

<!-- v -->

La même méthode ``save`` est utilisée pour enregistrer en base de données des modifications sur l'instance.

```python
>>> b.question_text ='Two scoops of django - Best practices'
>>> b.save()
```

Pour supprimer une instance, il suffit d'appeler la méthode ``delete()`` qui permet
de supprimer directement la ligne en base de données.

```python
# Suppression
>>> b.delete()
```

<!-- v -->

## Les concepts ``Manager`` & ``Queryset``

Pour récupérer une ou plusieurs instances, il faut construire un ``Queryset`` via un ``Manager`` associé au modèle.

<!-- v -->

### Qu'est ce qu'un ``Manager`` ?

Un ``Manager`` est l'interface à travers laquelle les opérations de requêtage en
base de données sont mises à disposition d'un modèle Django. Chaque modèle
possède un ``Manager`` par défaut accessible via la propriété ``objects``.

<!-- v -->

### Qu'est ce qu'un ``Queryset`` ?

Un ``Queryset`` représente une collection d'objets provenant de la base de
données. Cette collection peut être filtrée, limitée, ordonnée, … grâce à
des méthodes qui correspondent à des clauses SQL.

A partir d'un queryset il est possible d'obtenir un autre queryset plus spécialisé.
Un queryset est "paresseux" : la requête SQL n'est exécutée que lorsque les données sont
effectivement demandées.

<!-- v -->

### Exemples d'utilisation des QuerySet

- page de recherche, filtres (SELECT, éventuellement jointures)
- affichage des contenus en fonction d'un contexte : tâches de l'utilisateur ou d'une liste (SELECT JOIN)
- créer, mettre à jour ou supprimer des objets (CREATE, UPDATE, DELETE)
- opérations d'aggrégation : décompte, somme, moyenne, etc.

<!-- v -->

### Exemple

```
qs = Question.objects.all()
qs = qs.filter(question_text__startswith='Pourquoi')
qs = qs.exclude(brand=True)
count = qs.count()
```

Ici, la base de données n'est effectivement requêtée qu'à la dernière ligne.

Les trois premières préparent la requête, sans l'exécuter.

<!-- v -->

## Retrouver une liste d'instances

### Retrouver toutes les instances d'un modèle

```python
>>> question.objects.all()
```

<!-- v -->

### Retrouver une liste filtrée d'instances

Les méthodes de filtrage principalement utilisées sont ``filter`` et ``exclude``. Il est possible de les chaîner.

```python
question.objects.filter(
    pub_date__gte=date(2021, 1, 1)
).exclude(
    question_text__istartswith="Pourquoi"
)
```

<!-- v -->

### Retrouver une liste ordonnée d'instances

```python
question.objects.exclude(
    pub_date__lte=date(2021, 1, 1)
).order_by('question_text')
```

<!-- v -->

## ORM - Filtrage

* Les paramètres nommés sont le nom du champ et la valeur
* On peut ajouter derrière le nom du champ deux undescores et un lookup
    * `__iexact` pour une recherche insensible à la casse
    * `__contains` pour chercher à l'intérieur
    * `__lt`, `__lte`, ` __gt`,` __gte` pour les inégalités

<!-- v -->

* Avec deux undescores on peut aussi suivre une relation
* Il y a un **ET** logique entre les différentes conditions

_cf_ [documentation sur les querysets](https://docs.djangoproject.com/fr/stable/ref/models/querysets/)

```python
questions = question.objects.filter(
    question_text__startswith="Le"
)
questions = question.objects.filter(
    pub_date__year__lt=2021
).exclude(question_text__icontains="fleurs")
```

<!-- v -->

### Encapsuler plusieurs requêtes 

Pour l'opérateur **OU** ou des requêtes plus complexes, utiliser `django.db.models.F`
et `django.db.models.Q`, qui permettent des combinaisons avant exécution.

_cf_ [documentation sur les expressions](https://docs.djangoproject.com/fr/stable/ref/models/expressions/)

<!-- v -->

## Retrouver une instance en particulier

La méthode ``get`` permet de récupérer une instance particulière.

```python
>>> question.objects.get(pk=12)
```

La méthode ne peut retourner qu'une instance précise, il faut donc que le filtre
fourni ne soit pas ambigu.

Il faut veiller à filtrer sur un champ ``unique`` (ou
plusieurs champs uniques ensemble).

<!-- v -->

### Exceptions potentielles

* Si l'instance n'est pas trouvée, une exception ``question.DoesNotExist`` sera levée
(de manière générique : ``<Model>.DoesNotExist``).
* Si plusieurs instances ont été trouvées, l'exception levée sera
``question.MultipleObjectsReturned`` (``<Model>.MultipleObjectsReturned``).

<!-- v -->

## Référence à des objets associés

Pour les relations entre instances (``ForeignKey``, ``ManyToManyField``), Django
fournit un ``Manager`` spécifique nommé ``RelatedManager``. Il permet de :

* retrouver les instances liées par une ``ForeignKey``
* ajouter une liaison entre deux instances dans le cas d'un ``ManyToManyField``
* supprimer toutes les liaisons d'une instance vers d'autres

<!-- v -->

### Exemple

```python
question.choice_set.all()  # Sélectionne tous les choix d'une question
question.choice_set.count()  # Renvoie le nombre de choix
question.choice_set.all().delete()  # Supprime tous les choix de la question
```

`choice_set` est le `RelatedManager` qui permet d'accéder aux choix d'une question
