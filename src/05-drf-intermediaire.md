---
title: Django Rest Framework (partie 2)
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---

# Django Rest Framework (partie 2)

<!-- v -->

## Sommaire

- Quelques paramètres
- Authentification
- Permissions
- Filtrage

<!-- s -->

# Paramètres

<!-- v -->

## `REST_FRAMEWORK`

Paramètre unique, constitué d'un dictionnaire

Pour y accéder utiliser `api_settings`

```python
from rest_framework.settings import api_settings

print(api_settings.DEFAULT_AUTHENTICATION_CLASSES)
```

<!-- v -->

## Pagination

```python
REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 100
}
```

<!-- s -->

# Middleware

<!-- v -->

Altérer le traitement des requêtes de manière globale

```python
class SimpleMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # Code exécuté pour chaque requête avant la vue
        # ...

        response = self.get_response(request)

        # Code exécuté pour chaque requête après la vue
        # ...
        return response
```

Peut ausi travailler sur les exceptions et les templates

<!-- v -->

## Bonnes pratiques

Se place dans un fichier `middleware.py`, dans le projet ou dans une
application. Le chemin python de sa classe doit être référencé
dans le setting `MIDDLEWARE`, attention : l'ordre importe.

<https://docs.djangoproject.com/fr/stable/topics/http/middleware/>

<!-- v -->

## Middlewares Django intéressants

* `CommonMiddleware` : gère la réécriture du slash de fin
* `MessageMiddleware` : gère les messages en session
* `BrokenLinkEmailsMiddleware` : envoie les 404 par email
* `LocaleMiddleware` : sélectionne la locale
* `UpdateCacheMiddleware` et `FetchFromCacheMiddleware`

<https://docs.djangoproject.com/fr/stable/ref/middleware/>

<!-- s -->

# Authentification

<!-- v -->

## Accès limité aux utilisateurs équipe

Exemple : limiter l'accès à la liste des utilisateurs au seul staff

<!-- v -->

## Authentification par défaut

Par défaut, Django REST Framework active deux types d'authentification :

* par le [mécanisme de session](http://www.django-rest-framework.org/api-guide/authentication/#sessionauthentication) classique de Django
* [HTTP basic](http://www.django-rest-framework.org/api-guide/authentication/#basicauthentication)

<!-- v -->

### Paramétrer au niveau du projet

Dans les settings

```python
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ]
}
```

<!-- v -->

### Paramétrer au niveau d'une vue

```python
class ExampleView(APIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
```


<!-- v -->

## Authentification par token

L'[authentification par token](http://www.django-rest-framework.org/api-guide/authentication/#tokenauthentication) se paramètre dans les settings avec  [DEFAULT_AUTHENTICATION_CLASSES](http://www.django-rest-framework.org/api-guide/settings/#default_authentication_classes) :

```python
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        # ...
        'rest_framework.authentication.TokenAuthentication',
    )
}
```

Et ajouter l'app `rest_framework.authtoken` à `INSTALLED_APPS`.

<!-- v -->

### Création du token pour votre utilisateur

```python
from rest_framework.authtoken.models import Token

token = Token.objects.create(user=...)
print(token.key)
```

<!-- v -->

### Utilisation du token dans la requête

Puis indiquez le token dans le header de la requête `WWW-Authenticate: Token`

```sh
curl -X GET http://127.0.0.1:8000/api/example/ -H 'Authorization: Token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b'
```

<!-- v -->

Des extensions proposent d'autres types d'authentification comme OAuth2, JSON Web Token, etc.

<!-- v -->

## TP : Ajouter une authentification JWT

https://jpadilla.github.io/django-rest-framework-jwt/

<!-- s -->

# Permissions

<!-- v -->

## Ajouter une permission sur une vue API

```python
from rest_framework import permissions

class UserList(UserView, generics.ListCreateAPIView):
    permission_classes = (permissions.IsAdminUser, )
```

<!-- v -->

## Quelques [permissions](http://www.django-rest-framework.org/api-guide/permissions/) prêtes à l'emploi :

* [IsAuthenticated](http://www.django-rest-framework.org/api-guide/permissions/#isauthenticated)
* [IsAdminUser](http://www.django-rest-framework.org/api-guide/permissions/#isadminuser)
* [IsAuthenticatedOrReadOnly](http://www.django-rest-framework.org/api-guide/permissions/#isauthenticatedorreadonly)

Et on peut aussi créer ses propres permissions. 

<!-- v -->

## Tester les permissions

```python
@test_settings
class TestUserPermissions(APITestCase):
    def test_cannot_get_users_if_not_authenticated(self):
        response = self.client.get("/api/v1/users/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_can_get_users_if_staff(self):
        staff_user = User.objects.create_user("serveur", is_staff=True)
        self.client.force_login(staff_user)
        response = self.client.get("/api/v1/users/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
```

<!-- v -->

## Permission personnalisée

On veut limiter la création d'utilisateur super utilisateurs.  Il n'y a pas de
permission `IsSuperUserOrReadOnly`, voyons comment on peut l'ajouter.

<!-- v -->

### Définition

```python
# staff/permissions.py
from rest_framework import permissions

class IsSuperUserOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        else:
            return request.user.is_superuser
```

<!-- v -->

### Utilisation

```python
# staff/views.py
class UserList(UserView, generics.ListCreateAPIView):
    permission_classes = (permissions.IsAdminUser, IsSuperUserOrReadOnly)
```

<!-- v -->

### Le test

```python
def test_cannot_create_user_if_not_superuser(self):
    staff_user = User.objects.create_user("serveur", is_staff=True)
    self.client.force_login(staff_user)
    response = self.create_user()
    self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

def test_can_create_user_if_superuser(self):
    superuser = User.objects.create_user("serveur", is_staff=True,
                                         is_superuser=True)
    self.client.force_login(superuser)
    response = self.create_user()
    self.assertEqual(response.status_code, status.HTTP_201_CREATED)
```

Note : là encore il va falloir mettre à jour les tests existants.

<!-- s -->

## TP

* Ajouter un accès API vers les utilisateurs
* limiter l'accès à la liste des utitilisateurs aux seuls admins

### Étape 1

- Importer le modèle User depuis `django.contrib.auth`
- Créer un serializer
- Créer une vue pour afficher la liste des utilisateurs
- Limiter cette vue aux membres du staff

<!-- v -->

### Étape 2

être membres du staff est différent que être super utilisateur

- Créer une permission pour les super users

<!-- v -->

## TP partie 2

* configurer l'accès API des utilisateurs liste et détail
* autoriser tout utilisateur authentifié à voir et éditer seulement ses propres détails
* autoriser les users staff à seulement voir les détails de tous les users
* autoriser les superusers à éditer et voir les détails de tous les users

<!-- v -->

Indice : implémenter la méthode
[`has_object_permission()`](http://www.django-rest-framework.org/api-guide/permissions/#custom-permissions)
qui prend l'objet accédé en paramètre.

Pour tester à la main :

```console
$ curl -u username http://localhost:8000/staff/api/v1/users/
```

<!-- v -->

## TP partie 3

* Permettre aux utilisateurs de créer des livres

<!-- s -->

# Filtrage

<!-- v -->

## Filtrage sur l'url

Récupérer la liste des choix d'une question sur `api/v0/polls/1/choices/`

### L'url

```python
path('api/v0/polls/<int:question_id>/choices/',
     views.ChoiceAPIListView.as_view(),
     name='api_question_choices'),
```

<!-- v -->

### Redéfinir la queryset dans la vue correspondante

```python
class ChoiceAPIListView(ListCreateAPIView):
    # …            
    def get_queryset(self):
        """Liste of choices filter by question, if is in URL"""
        question_id = self.kwargs.get('question_id', '')
        if question_id:
            question = Question.objects.get(pk=question_id)
            return Choice.objects.filter(question=question)
        return Choice.objects.all()  # Renvoie tous les choix si pas de question
```

<!-- v -->

## Filtrage par QueryString

Par exemple, rechercher "Comment" dans le titre des questions

L'url ne change pas, on récupère la query string dans les paramètres de la requête.

<!-- v -->

### Redéfinir la queryset dans la vue

```python
class QuestionListView(APIView):
    # ...
    def get_queryset(self):
        queryset = super().get_queryset()
        search = self.request.query_params.get("search")
        if search:
            queryset = queryset.filter(question_text__icontains=search)
        return queryset
```

<!-- v -->

## Filtrage générique

Django REST Framework est livré avec des filtres déjà fait,
comme le [SearchFilter](http://www.django-rest-framework.org/api-guide/filtering/#searchfilter)
ou le [OrderingFilter](http://www.django-rest-framework.org/api-guide/filtering/#orderingfilter).

```python
from rest_framework import filters

class QuestionListView(APIView):
    # ...
    filter_backends = (filters.SearchFilter,)
    search_fields = ('question_text', )
    # ...
```

Plus besoin de surcharger `get_queryset`.

<!-- v -->

## Le test

```python
class TestAPI(TestCase):
    def setUp(self):
        Question.objects.create(question_text="Quelle boisson ?")
        Question.objects.create(question_text="Ton framework favori ?")
    # ...
    def test_search_question(self):
        response = self.client.get("/api/v0/polls/?search=framework")

        response_data = response.json()
        self.assertEqual(len(response_data["results"]), 1)
```

<!-- v -->

## Installer un moteur de filtre externe

[DjangoFilterBackend](https://www.django-rest-framework.org/api-guide/filtering/#djangofilterbackend)

en utilisant la bibliothèque python / django [Django filter](https://django-filter.readthedocs.io/)

<!-- s -->

# Questions ?

*Merci !*
