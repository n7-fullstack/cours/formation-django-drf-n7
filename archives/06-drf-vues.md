---
title: Django – DRF Vues
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---

# Vues de Django REST Framework


<!-- v -->

## Serializer simple

Jusqu'à présent : serializer de type `HyperlinkedModelSerializer`.

Créons un simple serializer.

```python
class FoodSerializer(serializers.Serializer):
    shrt_desc = serializers.CharField(max_length=200)
    long_desc = serializers.EmailField(max_length=200)

    def create(self, validated_data):
        return Food.objects.create(**validated_data)
```
<!-- v -->

## Fonction de vue

Transformer la vue liste pour renvoyer du JSON, à partir de la vue fonction

```python
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def food_list_json(request):
    if request.method == "GET":
        foods = Food.objects.all()
        serializer = FoodSerializer(foods, many=True, context={'request': request})
        response_data = {"results": serializer.data}
        return JsonResponse(response_data)
    elif request.method == "POST":
        data = JSONParser().parse(request)
        serializer = FoodSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)
```

<!-- v -->

## Urls

On peut garder les routeurs dans les urls. Le paramètre `name` permet de redéfinir l'url.

Pour rappel : le routeur fournit 2 urls `food-list` et `food-detail`

```python
urlpatterns = [
    # ...
    path('v1/', include(router.urls)),
    path('v2/foods/', views.food_list_json, name='food-list'),
    # ...
]
```

<!-- v -->

## Requêtes et réponses de DRF

Django REST Framework étend les classes HttpRequest et HttpResponse de Django :

* [Request](http://www.django-rest-framework.org/api-guide/requests/) 
ajoute notamment l'attribut `.data`, plus générique que `.POST`
* [Response](http://www.django-rest-framework.org/api-guide/responses/) 
ajoute de la négociation de contenu

Ces classes fonctionnent avec des *wrappers* de vues :

* le décorateur [@api_view](http://www.django-rest-framework.org/api-guide/views/#api_view)
* la classe [APIView](http://www.django-rest-framework.org/api-guide/views/#class-based-views)

<!-- v -->

## Utilisation dans nos vues

```python
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status


@api_view(['GET', 'POST'])
def food_list_json(request):
    if request.method == "GET":
        foods = Food.objects.all()
        serializer = FoodSerializer(foods, many=True, context={'request': request})
        response_data = {"results": serializer.data}
        return Response(response_data)
    elif request.method == "POST":
        serializer = FoodSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
```

Visiter [http://localhost:8000/api/v2/foods/](http://localhost:8000/api/v2/foods/) avec un navigateur et essayer la version HTML de l'API.

<!-- v -->
<!-- .slide: class="alternate" -->

## Vue détail

Modifier la vue `food_detail` pour qu'elle utilise `Request` et `Response`.

* s'assurer que nos tests passent toujours.  
* jouer avec la version HTML de l'API

<!-- s -->

# Vues de type classe

<!-- v -->

## Vue liste

```python
from rest_framework.views import APIView


class FoodList(APIView):
    def get(self, request, format=None):
        foods = Food.objects.all()
        serializer = FoodSerializer(foods, many=True)
        response_data = {"results": serializer.data}
        return Response(response_data)

    def post(self, request, format=None):
        serializer = FoodSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
```

<!-- v -->

## Vue détail

Modifier `food_detail` pour en faire une classe qui hérite de `APIView`


<!-- v -->

## Les classes mixins

Les opérations CRUD sont souvent les même d'une vue à l'autre. Django REST Framework propose des mixins qui implémentent ces opérations.

```python
from rest_framework import mixins
from rest_framework import generics
from staff.serializers import FoodSerializer


class FoodList(mixins.ListModelMixin, mixins.CreateModelMixin,
               generics.GenericAPIView):
    queryset = Food.objects.all()
    serializer_class = FoodSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)
```

<!-- v -->

- Modifier `food_detail` pour en faire une classe qui utilisent les mixins

    Voici les mixins dont vous aurez besoin :

    <table>
    <tr><th>Mixin</th><th>Méthode associée</th></tr>
    <tr><td><a href="http://www.django-rest-framework.org/api-guide/generic-views/#retrievemodelmixin">RetrieveModelMixin</a></td><td>retrieve</td></tr>
    <tr><td><a href="http://www.django-rest-framework.org/api-guide/generic-views/#updatemodelmixin">UpdateModelMixin</a></td><td>update</td></tr>
    <tr><td><a href="http://www.django-rest-framework.org/api-guide/generic-views/#destroymodelmixin">DestroyModelMixin</a></td><td>destroy</td></tr>
    </tbody>
    </table>

- Factoriser le code commun entre `FoodList` et `FoodDetail` dans une classe `FoodView`.

<!-- v -->

## Les vues génériques pré-mixées

Certaines combinaisons de mixins sont tellement courantes que DRF propose des vues qui incorporent déjà des mixins.

```python
class FoodList(FoodView, generics.ListCreateAPIView):
    pass
```
