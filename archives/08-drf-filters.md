---
title: DRF – Relations
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---

# Filtrage

<!-- v -->

## Le test

```python
class TestConso(MenuItemsMixin, TestCase):
    # ...
    def test_search_menu_title(self):
        self.create_menu_item("Coupe glacée", "6.50")
        self.create_menu_item("Coupe de champagne", "6.50")
        response = self.client.get("/api/v1/menu/?search=coupe")

        response_data = response.json()
        self.assertEqual(len(response_data["results"]), 2)
```

<!-- v -->

## Le ViewSet

```python
class MenuItemViewSet(viewsets.ModelViewSet):
    # ...
    def get_queryset(self):
        queryset = super().get_queryset()
        search = self.request.query_params.get("search")
        if search:
            queryset = queryset.filter(title__icontains=search)
        return queryset
```

<!-- v -->

# Filtrage générique

Django REST Framework est livré avec des filtres déjà fait, comme le [SearchFilter](http://www.django-rest-framework.org/api-guide/filtering/#searchfilter) ou le [OrderingFilter](http://www.django-rest-framework.org/api-guide/filtering/#orderingfilter).

```python
class MenuItemViewSet(viewsets.ModelViewSet):
    # ...
    filter_backends = (filters.SearchFilter,)
    search_fields = ('title', )
    # ...
```

Plus besoin de surcharger `get_queryset`.
