# Django / Django REST Framework

## 4 jours

---

# Faisons les présentations

Emmanuelle Helly, développeuse web et formatrice

----

## Sommaire



* Introduction
* L'environnement de développement
* Créer notre projet
* Ma première page
* Mon premier modèle
* L'administration Django
* Vues liste et détail
* Formulaire d'ajout
* Relier deux modèles

----

# Tutoriel fil rouge

Application de gestion de ressources pédagogiques

* Ressource : Titre, description, lien ou fichier, auteur, licence
* Auteur : Nom, email

---

# Introduction

----

## Qu'est-ce-que Django ?

* Framework en Python pour le Web
* Encourage le développement rapide et propre avec une conception pragmatique
* Django permet de construire des applications web rapidement et avec peu de code
* Malgré son haut niveau d'abstraction, il est toujours possible de descendre dans les couches

## Historique

* Créé en 2003 par le journal local de Lawrence (Kansas, USA), basé sur le langage Python créé en 1990
* Rendu Open Source (BSD) en 2005
* Django 1.x : compatible python 2 et 3
* Django 2.x : compatible python 3 uniquement
* Aujourd'hui utilisé par de très nombreuses entreprises/sites : Mozilla, Instagram, Pinterest, Disqus, Washington Times, ...

----

## Un framework, pour quoi faire ?

TODO: pourquoi un framework

----

## La philo de Django

* Framework en python pour le web
* Philosophie _keep it simple, stupid_ et _don't reapeat yourself_
* Architecture _Model_, _Template_, _View_

----

### Couplage faible

* Les différentes couches du framework sont indépendantes
* Le socle d'applications peut être réduit au strict minimum

### Peu de code à écrire

* Ecriture "automatique" de code
* Utilisation des possibilités d'introspection de Django

----

## Architecture _Model_, _Template_, _View_ ?

L'architecture de Django s'inspire du principe MVC (*Model, View, Controller*) ou plutôt MTV (*Model, Template, View*) :

* __Model__ : définition des objets. Django fournit un ORM pour accéder à la base de données
* __Template__ : affichage des données
* __View__ : fonction ou classes retournant des réponses HTTP

L'_URL dispatcher_ permet de faire correspondre des URLs sous forme d'expressions régulières à des vues.

----

## Les bonnes raisons d'utiliser Django

* Facile à installer
* Très complet
* Excellente documentation
* Modèles en Python et ORM faciles à utiliser
* Interface d'administration auto-générée
* Gestion de formulaires
* Serveur de développement inclus
* Extensible, nombreux modules existants
* Communauté autour du projet très active

---

# L'environnement de développement

----

## Versions utilisées

* Django 2.0+
* Python : 3.x
* Base de données : SQLite, PostgreSQL, MySQL

----

## Conventions de codage

La documentation précise certaines conventions de codage spécifiques à Django. La PEP 8 fait référence pour le reste.
[https://docs.djangoproject.com/fr/2.0/internals/contributing/writing-code/coding-style/]()

----

## Côté python

Python parcourt sys.path pour chercher les modules à importer

* Par défaut ce path contient les répertoires systèmes tels que ``/usr/lib/python``,
``/usr/local/lib/python``, ``~/.local/lib/python`` ainsi que le répertoire courant en général
* Comme tout module python, il faut que Django soit accessible dans le path pour pouvoir l'utiliser
* __Virtualenv__ permet de créer un environnement python en isolation du système,
c'est la méthode préférable pour développer avec python

----

## Installer et activer un _Virtualenv_

```!console
$ sudo apt install python3-venv
$ python3 -m venv venv
```

## Installation de Django

```!console
(venv) $ pip install django
```

----

# Créer notre projet

----

## Créer le projet

```!console
(venv) $ django-admin create monprojet
```

## Lancer le serveur

```!console
(venv) $ cd monprojet
(venv) $ ./manage.py runserver
```

----

### It works !

![](../img/00-run-server.png)

----

## Créer l'application

```!console
(venv) $ ./manage.py startapp teaching
```

----

## Structure du projet

```!console
monprojet
├── db.sqlite3
├── manage.py
├── monprojet
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
└── teaching
    ├── admin.py
    ├── apps.py
    ├── migrations
    ├── models.py
    ├── tests.py
    └── views.py
```

----

## Projet vs. Application


### Une application

Constitue une fonctionnalité (système de blog, application de sondage)

### Un projet

Contient les réglages et les applications pour un site Web particulier

**Un projet est une combinaison d'applications**

---

# Quelques réglages

----

## Activer l'application

```!python
# settings.py
INSTALLED_APPS = [
    'django.contrib.admin',
    ...
    'teaching',
]
```

## Mettre en français par défaut

```!python
LANGUAGE_CODE = 'fr-fr'
```

----

## Générer la structure de la base de données

```!console
$ ./manage.py migrate
```

_Remarque : par défaut Django utilise SQLite, en production PostgreSQL est plus indiqué._

---

# Ma première page

----

## Créer une vue page d'accueil

`home_view` reçoit la requête en paramètre, et renvoie une réponse.

```!python
### teaching/views.py
from django.http import HttpResponse

def home_view(request):
    return HttpResponse("Bienvenue !")
```

----

## Ajouter une URL

Déclarer un chemin et le relier à la vue

```!python
# monprojet/urls.py
from teaching import views

urlpatterns = [
    path('', views.home_view),
    # …
]
```

---

# Mon premier modèle

----

## Le modèle ressource et ses champs

En anglais : `CourseMaterial`

* title → CharField
* description → TextField
* url → URLField

[Documentation sur les champs](https://docs.djangoproject.com/fr/2.1/ref/models/fields/)

_todo : schéma de données ?_

----

## Déclarer le modèle

```!python
### teaching/models.py
class CourseMaterial(models.Model):
    """Model for CourseMaterial"""

    title = models.CharField(max_length=100)
    description = models.TextField(
        max_length=500, blank=True, null=True
    )
    url = models.URLField(
        max_length=200, blank=True, null=True
    )

    def __str__(self):
        return self.title
```

----

## Migrations de la base de données

* Générer le fichier de migration

```!console
$ ./manage.py makemigrations
```

* Appliquer les migrations à la base de données

```!console
$ ./manage.py migrate
```

Et maintenant ?

---

# L'administration Django

----

## Créer un super utilisateur

```!console
$ ./manage.py createsuperuser
```

Puis se connecter sur <http://127.0.0.1:8000/admin/>

----

## Déclarer le modèle dans l'admin

```!python
### teaching/admin.py
from django.contrib import admin
from teaching.models import CourseMaterial

admin.site.register(CourseMaterial)
```
----

## Modèle dans l'interface admin

![](../img/03-admin.png)


---

# Les URLs

----

## Inclusion d'*URLconf*

La configuration des URLs du projet inclue celles de chaque application

```!python
# monprojet/urls.py
urlpatterns = [
    # …
    path('teaching/', include(
            ('teaching.urls', 'teaching'), 
            namespace='teaching')
        ),
    # …
]
```

---

# Vues liste et détail

----

## Vue liste

Nous utilisons les classes génériques de Django

La vue

```!python
from django.views.generic import ListView
from teaching.models import CourseMaterial

class CourseMaterialList(ListView):
    """CourseMaterial list view"""
    model = Task
```

----

## URLs liste

Les URLs de l'app `teaching` sont incluses dans le projet et répondent à `/teaching/<chemin>`

```!python
### teaching/urls.py
from teaching import views
urlpatterns = [
    path('list', views.CourseMaterialList.as_view(), name='list'),
]
```

Aller sur l'adresse <http://127.0.0.1:8000/teaching/list>, une erreur est affichée : il manque un élément

----

## Le template :)

Il manquait le template (= Gabarit en français)

```jinja
{% if object_list %}
<dl>
    {% for course in object_list %}
    <li>{{ course }}
        <br><small>{{ course.description }}</small>
    </li>
    {% endfor %}
</dl>
{% else %}
<p>Aucune tâche !</p>
{% endif %}
```

----

## Vue détail

La vue

```!python
from django.views.generic import ListView, DetailView

class CourseMaterialDetail(DetailView):
    """CourseMaterial detail view"""
    model = CourseMaterial
```

Notes: À faire selon le temps restant

----

## URL pour la vue détail

En plus organisé qu'au début

```!python
### teaching/urls.py
urlpatterns = [
    path('coursematerial',
        views.CourseMaterialList.as_view(),
        name='coursematerial-list'
    ),
    path(
        'coursematerial/<int:pk>',
        views.CourseMaterialDetail.as_view(),
        name='coursematerial-detail'
    ),
]
```

----

## Le template

```jinja
<h1>{{ coursematerial.title }}</h1>

<dl>
    <dt>Description</dt>
    <dd>{{ coursematerial.description }}</dd>
    <dt>URL</dt>
    <dd>{{ coursematerial.url }}</dd>
</dl>
```

---

# Formulaire d'ajout

----

## Déclarer le formulaire

```!python
### teaching/forms.py
class CourseMaterialForm(forms.ModelForm):

    class Meta:
        model = CourseMaterial
        fields = ("title", "description", "url")
```

----

## La vue formulaire

```!python
### teaching/views.py
class CourseMaterialCreateView(CreateView):
    """CourseMaterial create view"""
    form_class = CourseMaterialForm
    template_name = 'teaching/coursematerial_form.html'
    success_url = '/teaching/coursematerial'
```

----

## Le template pour l'ajout

```!jinja
<!-- teaching/templates/teaching/coursematerial_form.html -->
<form method="post">
    {% csrf_token %} {{ form.as_p }}
    <input type="submit" value="Submit" />
</form>
```

---

# Relier deux modèles

----

## Les champs de relations

* ``models.ForeignKey`` → relation de type __1-N__
* ``models.ManyToManyField`` → relation de type __N-N__
* ``models.OneToOneField`` → relation de type __1-1__

----

## Le modèle Author

Pour cet exemple, relier une ressource à son auteur.

Le modèle `Author`

* name → CharField
* email → EmailField

Dans le modèle `CourseMaterial`, ajouter un champs de type clé étrangère

* author → ForeignKey

Notes: Autre exemple possible, ajouter des thématiques

----

## Déclaration du modèle

Déclaration du modèle `Author`

```!python
### teaching/models.py
class Author(models.Model):
    """Model for Author"""

    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
```

----

## Champs de relation entre les modèles

Ajout du champs de type `ForeignKey`

```!python
### teaching/models.py
class CourseMaterial(models.Model):
    # …
    author = models.ForeignKeyField(
        'Author', related_name='author',
        on_delete=models.CASCADE,
        blank=True, null=True
    )
```

----

## Migrations

Générer et appliquer les migrations

```!console
$ ./manage.py makemigrations
$ ./manage.py migrate
```

----

##  Interface d'administration

Déclarer le modèle dans l'admin

```!python
### teaching/admin.py
admin.site.register(Author)
```

Visiter <http://127.0.0.1:8000/admin/>

Notes: si on a le temps, ajouter l'auteur dans le formulaire d'ajout.

---

## Aller plus loin

* [Documentation](https://docs.djangoproject.com/fr/2.1/)
* [Tutorials](https://docs.djangoproject.com/fr/2.1/intro/)
* [Django packages](https://djangopackages.org/) : extensions

Et nous proposons des formations Django !

Notes: Cet atelier ne couvre pas tous les tutorials, il y en a d'autres

