---
title: Django – Modèles, champs et querysets
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---

# Initiation à Django (suite)

<!-- v -->

## Sommaire

- Champs de modèles
- Querysets
- Commandes django

<!-- v -->

## TP

TP sur les données de nutrition : données disponibles.

Base de données à importer grâce à un script python qu'on va construire.

https://gitlab.com/n7-fullstack/cours/django-nutricio

<!-- s -->

# Modèles et champs

<!-- v -->

## Déclaration d'un modèle

Les modèles représentent une structure de données stockée en base

* un **modèle** représente généralement une table en BDD et chaque champ une colonne.
* chaque **instance** (objet) de ce modèle sera une ligne dans cette table.

<!-- v -->

### Déclaration d'un aliment

```python
# nutrition/models.py
from django.db import models

class Food(models.Model):
    shrt_desc = models.CharField(max_length=100)
    validated = models.BooleanField(default=False)

    # Surcharge de la manière d'afficher un objet Book
    def __str__(self):
        return self.shrt_desc
```

Django attribue automatiquement une colonne `id` pour la clé primaire sur
cette table.

_cf_ [documentation sur les modèles](https://docs.djangoproject.com/fr/stable/ref/models/)

<!-- v -->

## API `_meta`

À partir de la classe d'un modèle (et non d'une instance de la classe), elle permet de :

- Lister les champs d'un modèle
- Accéder à un champs par un nom


```!consolel
Food._meta.get_fields('pub_date')
FoodPortion._meta.get_fields()
```

<!-- v -->

## Quelques options pour les modèles

L'ajout de la classe ``Meta`` dans un modèle permet de déclarer des *options de métadonnées* sur le modèle. Exemple :

```python
class Food(models.Model):
    ...

    class Meta:
        db_table = 'food'
        verbose_name = 'Food'
        verbose_name_plural = 'Foods'
        ordering = ['shrt_desc', ]
```

<!-- v -->

### Autres options

D'autres options permettent par exemple de :

* rendre le modèle abstrait
* demander à Django de ne pas gérer ce modèle en base de données
* préciser des critères de tri
* déclarer des permissions relatives au modèle

Note:

Ici on utilisera uniquement verbose_name et ordering.

Mentionner le fait que les noms de modèle sont declinés de leur nom système.

<!-- s -->

# Types de champs

<!-- v -->

## Quelques types

### Les champs texte

* `CharField` (une ligne avec longueur max)
* `TextField` (multiligne)
* `EmailField` (vérifie la syntaxe de l'adresse)

<!-- v -->

### Les champs pour les nombres

* `IntegerField` et `PositiveIntegerField`
* `FloatField`
* `DecimalField` (précision fixe, non soumis aux arrondis)
* `AutoField` (`IntegerField` incrémenté automatiquement)

<!-- v -->

### Booléens, dates, fichiers

* Les champs booléens :  `BooleanField` et `NullBooleandField`
* Les champs pour les dates :
    * `DateField`, `TimeField` et `DateTimeField`
    * `DurationField`
* Les champs pour la gestion des fichiers :
    * `FileField` et `ImageField`
    * `FilePathField`

<!-- v -->

## Options de champs

* ``verbose_name``: label du champ
* ``null`` : valeur NULL autorisée ou non en base de données
* ``blank`` : valeur vide autorisée lors de la validation du champ dans un formulaire
* ``default`` : valeur par défaut pour une nouvelle instance

<!-- v -->

* ``editable`` : le champ doit-il apparaître automatiquement dans les formulaires
* `choices` permet d'expliciter la liste de valeurs possibles
* `primary_key` est la clé primaire (remplace *id*)
* `unique` ajoute une contrainte d'unicité
* `validators` permet d'ajouter des contraintes de validation au niveau du modèle

_cf_ [documentation sur les champs de modèle](https://docs.djangoproject.com/fr/stable/ref/models/fields/#field-options)

Note:

Chaque type de champs possède ses propres propriétés. Cependant, certaines sont
communes et souvent utilisées comme :

<!-- s -->

# L'API QuerySet

<!-- v -->

La classe `QuerySet` est une classe de Django.

Peut être vue comme
une représentation abstraite d'une requête `SELECT` dans la base de données, c'est à dire
un ensemble de données *potentiel*.

La base de données est requêtée le plus tard possible, lorsque les données sont
effectivement demandées.

<!-- v -->

## Exemple

```
qs = Food.objects.all()
qs = qs.filter(shrt_desc__startswith='Pain')
qs = qs.exclude(brand=True)
count = qs.count()
```

Ici, la base de données n'est effectivement requêtée qu'à la dernière ligne ;
les trois premières préparent la requête, sans l'exécuter.

<!-- v -->

## Méthodes qui ne renvoient pas des `QuerySet`

#### `count()`
Renvoie le nombre d'éléments du `QuerySet`.

#### `exists()`
Indique si le `QuerySet` contient au moins un élément ou non.

**Rem:** Il est beaucoup plus efficace d'appeler `exists()` que de faire le test `count() > 0`

<!-- v -->

#### `first()`
Renvoie le premier objet du `QuerySet`

#### `last()`
Renvoie le dernier objet du `QuerySet`

<!-- v -->

#### `get(*lookup_params)`
- Cherche et renvoie l'objet unique qui correspond aux `lookup_params`
- Peut déclencher les exceptions `MultipleObjectsReturned` et `DoesNotExist`

<!-- v -->

#### `get_or_create(*lookup_params)`
- Comme `get`, mais créé l'objet s'il n'existe pas.
- Renvoie un tuple `(object, created)`, `created` indiquant si l'objet a été créé ou pas.  
- Peut déclencher l'exception `MultipleObjectsReturned`

<!-- v -->

#### `delete()`
Supprime tous les objets du `QuerySet`

#### `update_or_create(defaults=None, **kwargs)`

#### `update(*fields)`
Effectue un `update` des objets du `QuerySet` avec les champs fournis `fields`.

<!-- v -->

## Méthodes qui renvoient des `QuerySet`

#### `all()`
Renvoie tous les objets 

#### `filter(*lookup_params)`
Filtre les objets du `QuerySet` selon `lookup_params`. 

<!-- v -->

#### `reverse()`
Renverse l'ordre des objets du `QuerySet`

#### `distinct()`
Enlève les doublons parmi les objets du `QuerySet`. C'est généralement une opération
assez couteuse.

#### `order_by(*fields)`
Trie les objets du `QuerySet` selon les éléments du tuple `fields`

```
Food.objects.all().order_by('-shrt_desc', 'brand')
```

<!-- v -->

### Paramètres de lookup

#### `exact`
C'est le paramètre par défaut, par exemple `qs.filter(id=14)` est équivalent à
`qs.filter(id__exact=14)`

#### `iexact`
Comme `exact`, mais case-insensitive pour les strings

<!-- v -->

#### `contains`
Test de sous-chaîne, case-sensitive

```
Food.objects.filter(long_desc__contains='ain')
```

#### `icontains`
Test de sous-chaîne, case-insensitive

<!-- v -->

#### `in`
Test d'appartenance à une list, un tuple, un iterable

```
Food.objects.filter(id__in=[1, 3, 4])
```
<!-- v -->

#### `gt`, `gte`, `lt`, `lte`
Teste si le champ est supérieur strict, supérieur ou égal, inférieur strict, inférieur ou égal
à une valeur donnée

```
FoodPortion.objects.filter(id__gt=4)
```

<!-- v -->

#### `startswith`, `istartswith`, `endswith`, `iendswith`
Teste si le champ commence/fini par une chaîne donnée, case-sensitive/case-insensitive

<!-- s -->

# Django command

<!-- v -->

## Exemple d'une commande d'import

On souhaite faire la commande d'import des données d'aliment

<!-- v -->

# Relations

<!-- v -->

