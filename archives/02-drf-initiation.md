---
title: Initiation à Django REST Framework
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---

# Initiation à Django REST Framework

<!-- v -->

## Sommaire

- Ajouter DRF à Django
- Création API sur 1 modèle
- Première vue
- URLs de l'API
- Utilisation avec curl
- Deuxième modèle

<!-- s -->

# Ajouter DRF au projet

<!-- v -->

## Ajout dans les paquets requis

```console
pip install djangorestframework
```

Dans `requirements.txt`

```console
pip freeze > requirements.txt
```

## Ajout de l'app dans le projet

Dans `settings.py`

```python
INSTALLED_APPS = (
    ...
    'rest_framework',
)
```

<!-- s -->

# Premiers pas

Note:

Une approche par le haut

<!-- v -->

## Serializer

Les [**serializers**](http://www.django-rest-framework.org/api-guide/serializers/) convertissent des instances de modèles et de querysets en structures de données Python natives

```python
# polls/serializers.py
from rest_framework import serializers
from polls.models import Question

class QuestionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Question
        fields = ['url', 'question_text', 'pub_date']
```
<!-- v -->

## ViewSet

Un ViewSet permet de grouper les vues liste et détail

```python
# polls/views.py
from rest_framework import viewsets
from polls.models import Question
from polls.serializers import QuestionSerializer

class QuestionViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all().order_by("question_text")
    serializer_class = QuestionSerializer
```

<!-- v -->

## Configuration des URLs

Les routeurs génèrent une liste d'URLs à partir des viewsets enregistrés

```python
# polls/urls.py
from django.urls import include
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'question', views.QuestionViewSet)

urlpatterns = [
    # ...
    path('api/v1/', include(router.urls)),
]
```

<!-- v -->

## Pourquoi `v1` ?

Gérer les évolutions de l'API sans que les applications clientes ne soient impactés.

Pour permettre une rétro-compatibilité en cas d'évolution majeure et impactante  
→ créer une `v2` de l'API, et garder la `v1` accessible.

<!-- v -->

## Pagination

```python
# monprojet/settings.py
REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 10
}
```

'DEFAULT_PAGINATION_CLASS' : nombre d'objets et liens page précédente / suivante

<!-- v -->

## Tester

Utiliser l'interface web : [http://localhost:8000/polls/api/v1/](http://localhost:8000/polls/api/v1/)

Ou utiliser `curl`

<!-- v -->

### Requête GET

Obtenir la liste des questions

```console
$ curl -H 'Accept: application/json; indent=4' \
    http://localhost:8000/polls/api/v1/question/
```

<!-- v -->

### Requête POST

```console
curl -H 'Accept: application/json; indent=4' \
    -d "question_text=Youpi&pub_date=2020-01-11T11:00" \
    http://localhost:8000/polls/api/v1/question/
```

Renvoie

```json
{
    "url": "http://localhost:8000/polls/api/v1/question/4/",
    "question_text": "Youpi",
    "pub_date": "2020-01-11T11:00:00Z"
}
```

<!-- s -->

# Les serializers

<!-- v -->

## Sérialisation

* les [**serializers**](http://www.django-rest-framework.org/api-guide/serializers/) convertissent des instances de modèles et de querysets en structures de données Python natives
* ces structures de données Python peuvent ensuite être converties en JSON, XML, etc. par des [**renderers**](http://www.django-rest-framework.org/api-guide/renderers/)

![Sérialisation en JSON](../img/serializers1.png)

<!-- v -->

## Désérialisation

* les [**parsers**](http://www.django-rest-framework.org/api-guide/parsers/) convertissent les formats textuels en structures Python
* les [**serializers**](http://www.django-rest-framework.org/api-guide/serializers/) créent des instances de modèles à partir de ces structures

![Désérialisation depuis JSON](../img/serializers2.png)

<!-- s -->

# Deuxième modèle

<!-- v -->

## Serializer

```
# serializers.py
class ChoiceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Choice
        fields = ['url', 'question', 'choice_text', 'votes']
```

<!-- v -->

## Viewset

```
# views.py
class ChoiceViewSet(viewsets.ModelViewSet):
    queryset = Choice.objects.all().order_by('choice_text')
    serializer_class = ChoiceSerializer
```

## URL

```
# urls.py
router.register(r'choice', views.ChoiceViewSet)
```

<!-- s -->

# Conclusion

Avec peu de code, nous avons construit une API proposant 2 "end-points".

Note:

TODO: faire une présentation des concepts de REST
