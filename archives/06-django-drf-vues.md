---
title: Django – Relations
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---


# Vues fonctions

<!-- v -->

## Function-based views

Une vue *basée sur une fonction* est une fonction Python qui prend
en entrée une **requête HTTP** et retourne une **réponse HTTP**.

Cette réponse peut être une page HTML, un document XML, une redirection, une erreur
404, un fichier PDF ou zip généré à la volée…

<!-- v -->

## Vue liste

### Fonction de vue

Écrire une vue pour lister les aliments, avec un template, vers l'url `/api/views/food/`

```python
# nutrition/views.py
from django.shortcuts import render
from nutrition.models import Food

def food_list(request):
    # Récupération des aliments
    foods = Food.objects.all()[10:]

    # Définition d'un contexte à fournir au template
    context = {'foods': foods}

    # Rendu du gabarit
    return render(request, 'nutrition/food_list.html', context)
```

<!-- v -->

### URLs liste

Les URLs de l'app `nutrition` sont incluses dans le projet et répondent à `/nutrition/<chemin>`

```!python
# nutrition/urls.py
from nutrition import views
urlpatterns = [
    path('views/food', views.food_list, name='food-list'),
]
```

Aller sur l'adresse <http://127.0.0.1:8000/nutrition/list>, une erreur est affichée : il manque un élément

<!-- v -->

### Le template :)

Il manquait le template (= Gabarit en français)

```jinja
{% if object_list %}
<dl>
    {% for food in object_list %}
    <li>{{ food }}
        <br><small>{{ food.shrt_desc }}</small>
    </li>
    {% endfor %}
</dl>
{% else %}
<p>Aucun aliment !</p>
{% endif %}
```

<!-- v -->

## Intermède relations : optimisation des requêtes

Si on affiche les catégories pour chaque élément

```jinja
<ul>
    {% for cat in food.categories.all %}
    <li>{{ cat }}</li>
    {% endfor %}
</ul>
```

La requête doit être optimisée

```
queryset.prefetch_related("categories")
```

L'ORM ne fait qu'une seule requête supplémentaire avec une clause ``IN``

<!-- v -->

## Vue détail

```python
# nutrition/views.py
from django.shortcuts import get_object_or_404, render
from nutrition.models import Food

def food_detail(request, food_id):
    # Récupération des aliments
    food = get_object_or_404(Food, pk=food_id)

    # Définition d'un contexte à fournir au template
    context = {'food': food}

    # Rendu du gabarit
    return render(request, 'nutrition/food_detail.html', context)
```
<!-- v -->

### URL pour la vue détail

En plus organisé qu'au début

```!python
# nutrition/urls.py
urlpatterns = [
    path('views/food',
        views.FoodList.as_view(),
        name='food-list'
    ),
    path(
        'views/food/<int:pk>',
        views.FoodDetail.as_view(),
        name='food-detail'
    ),
]
```

<!-- v -->

### Le template

```jinja
<h1>{{ Food.title }}</h1>

<dl>
    <dt>Description</dt>
    <dd>{{ Food.shrt_desc }}</dd>
    <dt>URL</dt>
    <dd>{{ Food.url }}</dd>
</dl>
```

<!-- v -->

## Accès restreint

Décorateurs `login_required` ou `permission_required`

<!-- s -->

# Classes génériques

<!-- v -->

## Class-based views

Une vue *basée sur une classe* est une classe qui prend en entrée une **requête HTTP**
et retourne une **réponse HTTP** à travers sa méthode  `dispatch()` qui appelle la
méthode `get` ou `post()` selon le verbe HTTP.

<!-- v -->

## Vue liste

```!python
from django.views.generic import ListView
from nutrition.models import Food

class FoodList(ListView):
    """Food list view"""
    model = Food

    # redéfinir la méthode get_queryset()
    def get_queryset(self):
        queryset = super(FoodList, self).get_queryset()
        return queryset.prefetch_related("categories")
```

<!-- v -->

## Vue détail

```!python
from django.views.generic import ListView, DetailView

class FoodDetail(DetailView):
    """Food detail view"""
    model = Food
```

<!-- v -->

## Quelques classes de base

* ``View`` : classe *mère* et fournit le workflow vu précédemment
* ``TemplateView`` : faire le rendu d'un template avec un contexte
* ``FormView`` : gestion d'un formulaire en permettant une bonne organisation du code

<!-- v -->

### Afficher des instances

* ``ListView`` permet de lister très simplement des instances d'un modèle.
* ``DetailView`` permet d'afficher le détail d'une instance d'un modèle.

<!-- v -->

### Modifier des instances

* ``CreateView`` et ``UpdateView`` sont très utiles pour la création/modification
d'instance, de l'affichage du formulaire jusqu'à l'enregistrement.
* ``DeleteView`` facilite l'implémentation de vues pour la suppression d'instance.

Site pour un aperçu : <http://ccbv.co.uk/>
