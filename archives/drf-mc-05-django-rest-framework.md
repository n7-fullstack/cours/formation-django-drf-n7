<!-- .slide: class="title" data-background="#b5b42c" -->

# Django REST Framework

----

# Mise en place

**requirements.txt**

```console
djangorestframework==3.9.4
```

**settings.py**

```python
INSTALLED_APPS = (
    ...
    'rest_framework',
)
```

----

# Premiers pas

Une approche par le haut

## Serializer

```python
# conso/serializers.py
from rest_framework import serializers

class MenuItemSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MenuItem
        fields = ('title', 'price')
```

## ViewSet

```python
# conso/views.py
from rest_framework import viewsets

class MenuItemViewSet(viewsets.ModelViewSet):
    queryset = MenuItem.objects.all().order_by("title")
    serializer_class = MenuItemSerializer
```

----

## Configuration des URLs

```python
# conso/urls.py
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'menu', views.MenuItemViewSet)

urlpatterns = [
    # ...
    url(r'^api/v1/', include(router.urls)),
]
```

## Pagination

```python
# bistrot_project/settings.py
REST_FRAMEWORK = {
    'PAGE_SIZE': 10
}
```

----

## Tester

Exécuter les tests automatisés.

Tester avec curl :

```console
$ curl http://localhost:8000/conso/api/v1/menu/
```

Visiter [http://localhost:8000/conso/api/v1/](http://localhost:8000/conso/api/v1/) avec son navigateur.

----
<!-- .slide: class="alternate" -->

## Exercice

* Ajouter au model Order un champ `created_at` contenant [la date et l'heure de création](https://docs.djangoproject.com/fr/1.11/ref/models/fields/#django.db.models.DateField.auto_now_add) de la commande
* Ajouter un `ModelViewSet` pour les commandes en les ordonnant par date de création

**Aide :** Le test

```python
@test_settings
class TestOrder(MenuItemsMixin, TestCase):
    def setUp(self):
        self.create_menu_items()
        self.create_orders()

    def create_orders(self):
        # Factoriser la création des commandes dans un helper

    def test_get_orders_as_json(self):
        response = self.client.get("/api/v1/orders/")
        results = response.json()["results"]
        self.assertEqual(len(results), 2)
```

----

# Les **serializers**

## Sérialisation

* les [**serializers**](http://www.django-rest-framework.org/api-guide/serializers/) convertissent des instances de modèles et de querysets en structures de données Python natives
* ces structures de données Python peuvent ensuite être converties en JSON, XML, etc. par des [**renderers**](http://www.django-rest-framework.org/api-guide/renderers/)

![Sérialisation en JSON](../img/serializers1.png)

## Désérialisation

* les [**parsers**](http://www.django-rest-framework.org/api-guide/parsers/) convertissent les formats textuels en structures Python
* les [**serializers**](http://www.django-rest-framework.org/api-guide/serializers/) créent des instances de modèles à partir de ces structures

![Désérialisation depuis JSON](../img/serializers2.png)

----

# Exemple : lister les utilisateurs

## Le test

```python
@test_settings
class TestUser(TestCase):

    def test_get_users(self):
        User.objects.create_user("alice", "alice@example.com", "p4ssw0rd")
        User.objects.create_user("bob", "bob@example.com", "s3cr3t")

        response = self.client.get("/api/v1/users/")

        response_data = response.json()
        results = response_data["results"]
        self.assertEqual(len(results), 2)
        self.assertEqual(results[0]["username"], "alice")
```

----

## Le serializer

```python
# conso/serializers.py
from django.contrib.auth.models import User
from rest_framework import serializers

class UserSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=150)
    email = serializers.EmailField(max_length=150)
    password = serializers.CharField(max_length=128)
```

----

**Utilisation d'un serializer**

Une vue Django classique de type fonction

```python
# conso/views.py
def user_list(request):
    users = User.objects.all()
    serializer = UserSerializer(users, many=True)
    response_data = {"results": serializer.data}
    return JsonResponse(response_data)
```

Config des URLs

```python
# conso/users.py
urlpatterns = [
    # ...
    url(r'^api/v1/users/$', views.user_list),
    # ...
]
```

----

# Désérialisation : création d'un utilisateur

## Le test

```python
import json

@test_settings
class TestUser(APITestCase):
    def test_create_user(self):
        response = self.client.post("/api/v1/users/", json.dumps({
            'username': 'bob',
            'email': 'bob@example.com',
            'password': 'p4ssw0rd',
        }), content_type="application/json")
        user = User.objects.get(username='bob')
        self.assertTrue(user.check_password("p4ssw0rd"))
        response_data = response.json()
        self.assertEqual(response_data["username"], "bob")
```

----

## Le serializer

```python
class UserSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=150)
    email = serializers.EmailField(max_length=150)
    password = serializers.CharField(max_length=128)

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)
```

----

## La vue

```python
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def user_list(request):
    if request.method == "GET":
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        response_data = {"results": serializer.data}
        return JsonResponse(response_data)
    elif request.method == "POST":
        data = JSONParser().parse(request)
        serializer = UserSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)
```

----

# Simplification du test

### Utiliser APITestCase

```python
from rest_framework.test import APITestCase

@test_settings
class TestUser(APITestCase):

    def test_create_user(self):
        response = self.client.post("/api/v1/users/", {
            'username': 'bob',
            'email': 'bob@example.com',
            'password': 'p4ssw0rd',
        }, format="json")
        user = User.objects.get(username='bob')
        self.assertTrue(user.check_password("p4ssw0rd"))
        response_data = response.json()
        self.assertEqual(response_data["username"], "bob")
```

----

# Autres exemples d'utilisation

## Mise à jour d'une instance

```python
>>> user.email
'bob@example.com'
>>> serializer = UserSerializer(user)
>>> data = serializer.data
>>> data["email"] = "robert@example.com"
>>> update_serializer = UserSerializer(user, data=data)
>>> update_serializer.is_valid()
True
>>> update_serializer.save()
<User: bob>
>>> User.objects.get(username="bob")
>>> user.email
'robert@example.com'
```


## Suppression d'une instance

```python
>>> user.delete()
```

----
<!-- .slide: class="alternate" -->

## Exercices

- Déplacer le code relatif aux utilisateurs dans une nouvelle app `staff`
- Écrire une vue user_detail qui permet de récupérer un utilisateur, de le mettre à jour et de le supprimer
- Écrire au moins un test pour chacune de ces opérations

Pour la mise à jour vous devrez ajouter une méthode update au serializer :

```python
class UserSerializer(serializers.Serializer):
    # ...
    def update(self, instance, validated_data):
        instance.username = validated_data.get('username', instance.username)
        instance.email = validated_data.get('email', instance.email)
        new_password = validated_data.get('password')
        if new_password:
            instance.set_password(new_password)
        instance.save()
        return instance
```

----

# Requêtes et réponses

Django REST Framework étend les classes HttpRequest et HttpResponse de Django :

* [Request](http://www.django-rest-framework.org/api-guide/requests/) 
ajoute notamment l'attribut `.data`, plus générique que `.POST`
* [Response](http://www.django-rest-framework.org/api-guide/responses/) 
ajoute de la négociation de contenu

Ces classes fonctionnent avec des *wrappers* de vues :

* le décorateur [@api_view](http://www.django-rest-framework.org/api-guide/views/#api_view)
* la classe [APIView](http://www.django-rest-framework.org/api-guide/views/#class-based-views)

----

## Utilisation dans nos vues

```python
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status


@api_view(['GET', 'POST'])
def user_list(request):
    if request.method == "GET":
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        response_data = {"results": serializer.data}
        return Response(response_data)
    elif request.method == "POST":
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
```

Visiter [http://localhost:8000/staff/api/v1/users/](http://localhost:8000/staff/api/v1/users/) avec un navigateur et essayer la version HTML de l'API.

----
<!-- .slide: class="alternate" -->

## Exercice

Modifier la vue `user_detail` pour qu'elle utilise `Request` et `Response`.

* s'assurer que nos tests passent toujours.  
* jouer avec la version HTML de l'API

----

# Vues de type classe


```python
from rest_framework.views import APIView


class UserList(APIView):
    def get(self, request, format=None):
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        response_data = {"results": serializer.data}
        return Response(response_data)

    def post(self, request, format=None):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
```

----
<!-- .slide: class="alternate" -->

## Exercice

Modifier `user_detail` pour en faire une classe qui hérite de `APIView`

* s'assurer que nos tests passent toujours

----

# Les classes mixins

Les opérations CRUD sont souvent les même d'une vue à l'autre. Django REST Framework propose des mixins qui implémentent ces opérations.

```python
from rest_framework import mixins
from rest_framework import generics
from staff.serializers import UserSerializer


class UserList(mixins.ListModelMixin, mixins.CreateModelMixin,
               generics.GenericAPIView):
    queryset = User.objects.all().order_by("username")
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)
```

----
<!-- .slide: class="alternate" -->

## Exercice

- Modifier `user_detail` pour en faire une classe qui utilisent les mixins

Voici les mixins dont vous aurez besoin :

<table>
<tr><th>Mixin</th><th>Méthode associée</th></tr>
<tr><td><a href="http://www.django-rest-framework.org/api-guide/generic-views/#retrievemodelmixin">RetrieveModelMixin</a></td><td>retrieve</td></tr>
<tr><td><a href="http://www.django-rest-framework.org/api-guide/generic-views/#updatemodelmixin">UpdateModelMixin</a></td><td>update</td></tr>
<tr><td><a href="http://www.django-rest-framework.org/api-guide/generic-views/#destroymodelmixin">DestroyModelMixin</a></td><td>destroy</td></tr>
</tbody>
</table>

- Factoriser le code commun entre `UserList` et `UserDetail` dans une classe `UserView`.

----

# Les vues génériques pré-mixées

Certaines combinaisons de mixins sont tellement courantes que DRF propose des vues qui incorporent déjà des mixins.

```python
class UserList(UserView, generics.ListCreateAPIView):
    pass
```

----
<!-- .slide: class="alternate" -->

## Exercice

Modifier `UserDetail` pour qu'elle hérite de 
[`RetrieveUpdateDestroyAPIView`](http://www.django-rest-framework.org/api-guide/generic-views/#retrieveupdatedestroyapiview).

----

# Authentification et permissions

Exemple : limiter l'accès à la liste des utilisateurs au seul staff

### Le test

```python
@test_settings
class TestUserPermissions(APITestCase):
    def test_cannot_get_users_if_not_authenticated(self):
        response = self.client.get("/api/v1/users/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_can_get_users_if_staff(self):
        staff_user = User.objects.create_user("serveur", is_staff=True)
        self.client.force_login(staff_user)
        response = self.client.get("/api/v1/users/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
```

### Exécuter seulement ce nouveau test case

```console
$ ./manage.py test staff.tests.TestUserPermissions
```

----

# Permissions

```python
from rest_framework import permissions

class UserList(UserView, generics.ListCreateAPIView):
    permission_classes = (permissions.IsAdminUser, )
```


## Exercice

Le nouveau test passe, mais à présent d'autres tests échouent.  Mettre à jour
les autres tests pour prendre en compte les nouvelles contraintes.

## Quelques [permissions](http://www.django-rest-framework.org/api-guide/permissions/) prêtes à l'emploi :

* [IsAuthenticated](http://www.django-rest-framework.org/api-guide/permissions/#isauthenticated)
* [IsAdminUser](http://www.django-rest-framework.org/api-guide/permissions/#isadminuser)
* [IsAuthenticatedOrReadOnly](http://www.django-rest-framework.org/api-guide/permissions/#isauthenticatedorreadonly)

Et on peut aussi créer ses propres permissions. 

----

# Permission personnalisée

On veut limiter la création d'utilisateur super utilisateurs.  Il n'y a pas de
permission `IsSuperUserOrReadOnly`, voyons comment on peut l'ajouter.

## Le test

```python
def test_cannot_create_user_if_not_superuser(self):
    staff_user = User.objects.create_user("serveur", is_staff=True)
    self.client.force_login(staff_user)
    response = self.create_user()
    self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

def test_can_create_user_if_superuser(self):
    superuser = User.objects.create_user("serveur", is_staff=True,
                                         is_superuser=True)
    self.client.force_login(superuser)
    response = self.create_user()
    self.assertEqual(response.status_code, status.HTTP_201_CREATED)
```

----

## Définition

```python
# staff/permissions.py
from rest_framework import permissions

class IsSuperUserOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        else:
            return request.user.is_superuser
```

### Utilisation

```python
# staff/views.py
class UserList(UserView, generics.ListCreateAPIView):
    permission_classes = (permissions.IsAdminUser, IsSuperUserOrReadOnly)
```

Note : là encore il va falloir mettre à jour les tests existants.

----
<!-- .slide: class="alternate" -->

## Exercice

* autoriser tout utilisateur authentifié à voir et éditer seulement ses propres détails
* autoriser les users staff à seulement voir les détails de tous les users
* autoriser les superusers à éditer et voir les détails de tous les users

Indice : implémenter la méthode
[`has_object_permission()`](http://www.django-rest-framework.org/api-guide/permissions/#custom-permissions)
qui prend l'objet accédé en paramètre.

Pour tester à la main :

```console
$ curl -u username http://localhost:8000/staff/api/v1/users/
```

----

# Middleware

Altérer le traitement des requêtes de manière globale

```python
class SimpleMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # Code exécuté pour chaque requête avant la vue
        # ...

        response = self.get_response(request)

        # Code exécuté pour chaque requête après la vue
        # ...
        return response
```

Peut ausi travailler sur les exceptions et les templates

----

## Bonnes pratiques

Se place dans un fichier `middleware.py`, dans le projet ou dans une
application. Le chemin python de sa classe doit être référencé
dans le setting `MIDDLEWARE`, attention : l'ordre importe.

<https://docs.djangoproject.com/fr/stable/topics/http/middleware/>


## Middlewares Django intéressants

* `CommonMiddleware` : gère la réécriture du slash de fin
* `MessageMiddleware` : gère les messages en session
* `BrokenLinkEmailsMiddleware` : envoie les 404 par email
* `LocaleMiddleware` : sélectionne la locale
* `UpdateCacheMiddleware` et `FetchFromCacheMiddleware`

<https://docs.djangoproject.com/fr/stable/ref/middleware/>


----
<!-- .slide: class="alternate" -->

## Tutoriel

Limiter l'accès à l'API aux utilisateurs connectés

----

# Authentification

Par défaut, Django REST Framework active deux types d'authentification :

* par le [mécanisme de session](http://www.django-rest-framework.org/api-guide/authentication/#sessionauthentication) classique de Django
* [HTTP basic](http://www.django-rest-framework.org/api-guide/authentication/#basicauthentication)

Une [authentification par token](http://www.django-rest-framework.org/api-guide/authentication/#tokenauthentication) est aussi disponible mais il faut l'activer dans les settings avec [DEFAULT_AUTHENTICATION_CLASSES](http://www.django-rest-framework.org/api-guide/settings/#default_authentication_classes) :

```python
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        # ...
        'rest_framework.authentication.TokenAuthentication',
    )
}
```

Et ajouter l'app `rest_framework.authtoken` à `INSTALLED_APPS`.

Des extensions proposent d'autres types d'authentification comme OAuth2, JSON Web Token, etc.

----

# Les relations imbriquées

Jusqu'ici une commande est représentée comme ceci :

```json
{
    "creation_time": "2017-05-31T13:04:57.244660Z",
    "items": [
        "http://localhost:8000/conso/api/v1/menu/1",
        "http://localhost:8000/conso/api/v1/menu/2",
        "http://localhost:8000/conso/api/v1/menu/3"
    ]
}
```

On peut ajouter un serializer en tant que champ d'un autre serializer :

```python
class OrderSerializer(serializers.HyperlinkedModelSerializer):
    items = MenuItemSerializer(many=True)
    
    class Meta:
        model = Order
        fields = ('creation_time', 'items')
```

----

On a alors le champs des entités liées directement dans la représentation :

```json
{
    "creation_time": "2017-05-31T13:04:57.244660Z",
    "items": [
        {
            "title": "Café", "price": "1.50"
        },
        {
            "title": "Thé", "price": "1.80"
        },
        {
            "title": "Demi", "price": "2.50"
        }
    ]
}
```

Il existe plusieurs manière de représenter des entités liées : [lister les clés primaire](http://www.django-rest-framework.org/api-guide/relations/#primarykeyrelatedfield), afficher une [représentation textuelle](http://www.django-rest-framework.org/api-guide/relations/#stringrelatedfield), etc.

----

# Champs calculés

Pour ajouter un champ calculé, il suffit d'ajouter une méthode au modèle qui prends seulement `self` en argument :


```python
class Order(models.Model):
    creation_time = models.DateTimeField(auto_now_add=True)
    items = models.ManyToManyField(MenuItem, related_name="orders")

    def total_price(self):
        return sum(item.price for item in self.items.all())
```

On peut alors ajouter le champs au serializer :

```python
class OrderSerializer(serializers.HyperlinkedModelSerializer):
    items = MenuItemSerializer(many=True)

    class Meta:
        model = Order
        fields = ('creation_time', 'items', 'total_price')
```

----
<!-- .slide: class="alternate" -->

## Exercice

Pour chaque élément du menu, ajouter un champ `order_count` indiquant le nombre de commandes dans lesquelles il figure.

----

# Les ViewSets

* Type de vue qui ne gère pas directement les méthodes HTTP comme GET ou POST 
mais fournit des actions comme `list` ou `create`
* On lie ces actions à des méthodes HTTP au moment de finaliser la vue avec `as_view`
* Ils permettent donc de faire facilement du CRUD sur une (ou plusieurs) ressource

----
**On regroupe nos vues dans un viewset**

```python
from rest_framework import viewsets
from staff.permissions import UserPermission

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by("username")
    serializer_class = UserSerializer
    permission_classes = (UserPermission, )  # Nouvelle classe de permission
```

**Mise à jour de la conf des URLs**


```python
user_list = views.UserViewSet.as_view({'get': 'list', 'post': 'create'})
user_detail = views.UserViewSet.as_view({'get': 'retrieve', 'put': 'update',
                                         'delete': 'destroy'})
urlpatterns = [
    url(r'^api/v1/users/$', user_list),
    url(r'^api/v1/users/(?P<pk>\d+)/$', user_detail),
]
```

----

**Nouvelle classe de permission**

```python
class UserPermission(BasePermission):
    def has_permission(self, request, view):
        if view.action not in ("list", "create"):
            return True
        if request.method in SAFE_METHODS:
            return request.user.is_staff
        else:
            return request.user.is_superuser

    def has_object_permission(self, request, view, obj):
        if request.user.is_superuser:
            return True
        if request.user.is_staff and request.method in SAFE_METHODS:
            return True
        return obj == request.user
```

----

# Les routeurs

Les routeurs génèrent une liste d'URLs à partir des viewsets enregistrés.

**Nouvelle configuration d'URLs**

```python
from django.conf.urls import url, include
from rest_framework import routers
from staff import views

router = routers.DefaultRouter()
router.register("users", views.UserViewSet)


urlpatterns = [
    url(r'^api/v1/', include(router.urls)),
]
```

----

## Actions personnalisées

[Définir des actions](http://www.django-rest-framework.org/api-guide/routers/#extra-link-and-actions) autres que les 5 actions standards.

Exemple : on ajoute une action qui applique un pourcentage de réduction à un item.

## Le test

```python
def test_discount(self):
    item = self.create_menu_item("Café", "1.50")

    response = self.client.post(f"/api/v1/menu/{item.pk}/discount/", {
        "discount_rate": 20,
    }, format="json")

    self.assertEqual(response.status_code, 200)
    item = MenuItem.objects.get(pk=item.pk)
    self.assertEqual(item.price, Decimal("1.20"))
```

----

## La route personnalisée

```python
class MenuItemViewSet(viewsets.ModelViewSet):
    queryset = MenuItem.objects.all().order_by("title")
    serializer_class = MenuItemSerializer

    @detail_route(methods=['post'])
    def discount(self, request, pk):
        discount_rate = request.data["discount_rate"]

        item = MenuItem.objects.get(pk=pk)
        discount_amount = item.price * Decimal(discount_rate) / Decimal(100)
        item.price = item.price - discount_amount
        item.save()

        serializer = MenuItemSerializer(item)
        return Response(serializer.data)
```

----
<!-- .slide: class="alternate" -->

## Exercice

Mettre en place une route personalisée qui applique un discount à tous les MenuItem.

* utiliser le décorateur [@list_route](http://www.django-rest-framework.org/api-guide/viewsets/#marking-extra-actions-for-routing).
* faire une première implémentation à base de boucle sur tous les menu items
* réfléchir à la performance de notre solution.
* considérer l'utilisation des [F() expressions](https://docs.djangoproject.com/en/1.11/ref/models/expressions/#f-expressions)

----

# Filtrage

## Le test

```python
class TestConso(MenuItemsMixin, TestCase):
    # ...
    def test_search_menu_title(self):
        self.create_menu_item("Coupe glacée", "6.50")
        self.create_menu_item("Coupe de champagne", "6.50")
        response = self.client.get("/api/v1/menu/?search=coupe")

        response_data = response.json()
        self.assertEqual(len(response_data["results"]), 2)
```

## Le ViewSet

```python
class MenuItemViewSet(viewsets.ModelViewSet):
    # ...
    def get_queryset(self):
        queryset = super().get_queryset()
        search = self.request.query_params.get("search")
        if search:
            queryset = queryset.filter(title__icontains=search)
        return queryset
```

----

# Filtrage générique

Django REST Framework est livré avec des filtres déjà fait, comme le [SearchFilter](http://www.django-rest-framework.org/api-guide/filtering/#searchfilter) ou le [OrderingFilter](http://www.django-rest-framework.org/api-guide/filtering/#orderingfilter).

```python
class MenuItemViewSet(viewsets.ModelViewSet):
    # ...
    filter_backends = (filters.SearchFilter,)
    search_fields = ('title', )
    # ...
```


Plus besoin de surcharger `get_queryset`.

----

# Et aussi...

* [Le throttling](http://www.django-rest-framework.org/api-guide/throttling/) pour limiter le nombre d'accès
* [Les schemas](http://www.django-rest-framework.org/api-guide/schemas/) pour fournir une description de son API
* customiser [la pagination](http://www.django-rest-framework.org/api-guide/pagination/)
* l'authentification [par tokens](http://www.django-rest-framework.org/api-guide/authentication/#tokenauthentication)
* et bien [plus encore](http://www.django-rest-framework.org/#api-guide)...
