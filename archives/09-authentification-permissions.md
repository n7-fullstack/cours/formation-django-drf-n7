---
title: DRF – Authentification et permissions
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---

# Authentification et permissions

<!-- v -->

## Accès restreint de la liste des utilisateurs

### Le test

```python
@test_settings
class TestUserPermissions(APITestCase):
    def test_cannot_get_users_if_not_authenticated(self):
        response = self.client.get("/api/v1/users/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_can_get_users_if_staff(self):
        staff_user = User.objects.create_user("serveur", is_staff=True)
        self.client.force_login(staff_user)
        response = self.client.get("/api/v1/users/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
```

<!-- v -->

### Exécuter seulement ce nouveau test case

```console
$ ./manage.py test staff.tests.TestUserPermissions
```

<!-- s -->

# Permissions

<!-- v -->

```python
from rest_framework import permissions

class UserList(UserView, generics.ListCreateAPIView):
    permission_classes = (permissions.IsAdminUser, )
```

<!-- v -->

## Mettre à jour les tests

Le nouveau test passe, mais à présent d'autres tests échouent.  Mettre à jour
les autres tests pour prendre en compte les nouvelles contraintes.

<!-- v -->

## Quelques [permissions](http://www.django-rest-framework.org/api-guide/permissions/) prêtes à l'emploi :

* [IsAuthenticated](http://www.django-rest-framework.org/api-guide/permissions/#isauthenticated)
* [IsAdminUser](http://www.django-rest-framework.org/api-guide/permissions/#isadminuser)
* [IsAuthenticatedOrReadOnly](http://www.django-rest-framework.org/api-guide/permissions/#isauthenticatedorreadonly)

Et on peut aussi créer ses propres permissions. 

<!-- s -->

# Authentification

<!-- v -->

## Authentification par défaut de DRF

Par défaut, Django REST Framework active deux types d'authentification :

* par le [mécanisme de session](http://www.django-rest-framework.org/api-guide/authentication/#sessionauthentication) classique de Django
* [HTTP basic](http://www.django-rest-framework.org/api-guide/authentication/#basicauthentication)

<!-- v -->

## Authentification par token

Une [authentification par token](http://www.django-rest-framework.org/api-guide/authentication/#tokenauthentication) est aussi disponible mais il faut l'activer dans les settings avec [DEFAULT_AUTHENTICATION_CLASSES](http://www.django-rest-framework.org/api-guide/settings/#default_authentication_classes) :

```python
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        # ...
        'rest_framework.authentication.TokenAuthentication',
    )
}
```

Et ajouter l'app `rest_framework.authtoken` à `INSTALLED_APPS`.

Des extensions proposent d'autres types d'authentification comme OAuth2, JSON Web Token, etc.
