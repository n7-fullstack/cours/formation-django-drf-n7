---
title: DRF – Relations
author: Emmanuelle Helly
separator: <!-- s -->
verticalSeparator: <!-- v -->
theme: white
css: theme/fullstack.css
---

# Les relations imbriquées

<!-- v -->

## Catégories d'un aliment

Un aliment

```json
{
    "id": 597,
    "shrt_desc": "Abondance",
    "long_desc": "Abondance, fromage"
},
```

On peut ajouter un serializer en tant que champ d'un autre serializer :

```python
class OrderSerializer(serializers.HyperlinkedModelSerializer):
    items = CategorySerializer(many=True)
    
    class Meta:
        model = Order
        fields = ('creation_time', 'items')
```

Il existe plusieurs manière de représenter des entités liées : [lister les clés primaire](http://www.django-rest-framework.org/api-guide/relations/#primarykeyrelatedfield), afficher une [représentation textuelle](http://www.django-rest-framework.org/api-guide/relations/#stringrelatedfield), etc.
