# Django Filage

## Résumé

- TP / Notre projet cobaye
- Projets / travail sur les projets

## Journée 1 : initiation Django

- Rappels python
- Introduction à Django
  - Framework
  - Exemples d'applications Django
  - Projet, apps… l'architecture
  - MVC → MTV
  - Formulaires / Models
- TP : création virtualenv, projet + app
- Une première page
  - URL dispatcher
  - Les vues
  - Templates
- TP : première vue index et url
- Formulaires
  - Forms et fields
  - Widgets
- Models
  - Models et champs
  - ORM
  - les types de champs
  - les migrations
- TP : premier modèle
  - django shell
- TP : créer des contenus, lister des contenus depuis le shell django

## Journée 2 : Django initiation suite

- Admin Django
- TP : configurer l'admin

- Relations entre les modèles
  - Types de relations
  - ORM et relations
- TP : deuxième modèle, la liste de tâches

- Django comme application web complète (frontend internalisé)
  - Vues / Models
  - Templates / Models
  - Forms / Models
- TP : Créer une vue liste, détail, formulaire d'ajout
- Vues et templates : extends, block, tags et filtres
- TP : finaliser les vues liste et détail et leurs templates associés

- Django comme application d'API Web
  - Vue json
  - Installer Django-rest-framework
  - Vue et serializer

## Journée 3 : Django intermédiaire

### Matin

- Outils pour le dev
  - Django Debug Toolbar
  - Django extensions
- TP : Installer un projet django externe
- Écrire des tests
  - Pourquoi des tests
  - Types de tests
  - Tests avec Django
- TP : écrire les tests pour la vue page d'accueil, la vue liste et la vue détail

### Après-midi

- Django / DRF comme API REST (frontend externalisé)
  - Notions sur les APIs REST
  - Vues liste / détail en json
- TP : créer une vue liste qui renvoie du json sur "/api/v0"
- Django Rest Framework
  - Premiers pas avec DRF
  - Vues
  - Serialisers
- TP : créer une vue liste qui renvoie du json en utilisant DRF

## Suite

- django commands : importer les données depuis une base
- Statics / Projets / personnaliser l'apparence avec des CSS et des images
- Fichiers
- Geodjango
- DRF : relations

## Journée 3

- Django : Formulaires
- DRF : Vue API lecture
- DRF : Vue API Écriture
- DRF : viewset
- Django : Users, authentification et permissions
- DRF : authentification basique

## Journée 4

- DRF : relations imbriquées
- DRF : filtres
    - TP Ajouter des filtres aux serializers existants
    - montrer qu'on peut mettre en place un outil de filtre extérieur
- Déployer une application Django

## Attention

- include urls (ne pas oublier "'")
- django 3 : settings → 'bookshare.apps.BookshareConfig'
- view json classique : JsonResponse (safe=False)
- json.loads(data) en utilisant le serializer de Django

## À faire chez soi

- créez votre application django et une première API
- si vous voulez aller plus loin avec django : tutoriel

## Projets / Exemples

Créer l'appli selon le fil rouge, pour réfléchir et expérimenter : ce sera un brouillon que vous pourrez jeter à la fin du cours, car vous devrez sûrement repenser des choses depuis zero pour construire votre vraie appli.

Champs communs : nom ou titre, description / résumé, date

* Appli mobile cartographique de partage de points d’intérêt
    * modèles : profil voyageur, point d'intérêt, commentaires
* Appli mobile sociale de photos pour des conseils de mode
    * modèles : profil photographe, photo, commentaires
* Appli sportive collaborative
    * modèles : profil sportif, sport, performances
* Appli partage de livres
    * modèles : profil utilisateur, livre, collection, prêt / transaction / échange
* Enstein battle 
    * modèles : profil joueur, questions, réponses

## Déroulé TP

### Jour1 : matin

- créer le projet
- créer l'app
- explorer les fichiers
- lancer le serveur
- initialiser le dépôt git (resource : https://www.gitignore.io/)

### La première vue

- créer une vue http "hello"
- déclarer une url dans l'app
- inclure les urls de l'app dans les urls du projet

### Les modèles

- configurer la base de donnée : laisser sqlite pour le moment
- lancer les migrations
- créer un modèle avec 2 champs : titre et booléen
- générer les migrations
- lancer les migrations
- créer un superuser
- se connecter à l'admin
- relier l'admin au modèle
- explorer avec django admin, créer des contenus
- explorer avec django shell
- créer un deuxième modèle

### Jour1 : après-midi

### Vues listes et élément

- créer une vue pour accéder à l'élément
- créer une vue liste et son template

### Vue json / DRF

- créer une vue liste en json
- installer DRF
- créer un serializer et son viewset


## Resources

