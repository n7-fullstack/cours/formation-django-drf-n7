# Formation Django / DRF

Formation Django / DRF pour la formation Développeur Fullstack à l'N7.

## Sommaire

- [Introduction](src/00-intro.md)
- [Django initiation](src/01-django-initiation.md)
- [Django REST Framework initiation](src/old/02-drf-initiation.md)
- [Django jour 2 : Champs, Relation ForeignKey, queries, django commande](src/03-django-models-fields-queries.md)
- [Django jour 3 : tests, requêtes et réponses](src/old/04-django-tests.md)
- [Django jour 3 bis : Relations ManyToMany, OneToOne, queries](src/old/05-django-relations.md)
- Django jour 4 (à venir)

## Utiliser

Ce support utilise [revealJS](https://revealjs.com/) et [reveal-md](https://github.com/webpro/reveal-md).

Cloner le dépôt, installer le paquet reveal-md avec npm, puis le lancer.

```
git clone
npm install
```

Lancer les slides dans le navigateur

```
npm start
```

Renommer le projet, au niveau du dossier ainsi que dans `package.json`.

Remplacer l'url du dépôt par votre nouveau dépôt

```
git remote set-url origin <url>
```

## Générer le pdf

```
npm run build:pdf
```

## Licence et Crédits

<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/fr/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/3.0/fr/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/fr/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 3.0 France</a>.

Les exemples sont [issus du tutorial Django](https://docs.djangoproject.com/fr/3.0/intro/).
